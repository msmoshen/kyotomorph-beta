CXX := g++
CXXFLAGS := -g -m64 -w # -pg

OBJECTS := morph.o dic.o tagger.o pos.o sentence.o feature.o node.o tools.o
HEADERS := common.h dic.h tagger.h pos.h sentence.h feature.h node.h parameter.h

MKDARTS_OBJECTS := mkdarts.o pos.o
MKDARTS_HEADERS := tagger.h pos.h

.cc.o: $(HEADERS)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

all: morph mkdarts

morph: $(OBJECTS) $(HEADERS)
	$(CXX) $(CXXFLAGS) -o $@ $(OBJECTS)

mkdarts: $(MKDARTS_OBJECTS) $(MKDARTS_HEADERS)
	$(CXX) $(CXXFLAGS) -o $@ $(MKDARTS_OBJECTS)

clean:
	rm -f $(OBJECTS) $(MKDARTS_OBJECTS) morph mkdarts
