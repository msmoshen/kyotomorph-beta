#ifndef PARAMETER_H
#define PARAMETER_H

#include "common.h"

namespace Morph {

class Parameter {
public:
	bool unknown_word_detection;
	bool shuffle_training_data;
	bool debug;
	unsigned int unk_max_length;
	unsigned int iteration_num;
	std::string darts_filename;
	std::string dic_filename;
	std::string pos_filename;
	std::string ftmpl_filename;
	std::string subftmpl_filename;

	std::string master_path;

	unsigned int freq_threshold;
	unsigned int generic_switch;
	unsigned int config_mode;
	unsigned int division;
	unsigned int N;
	unsigned int N_redundant;

	unsigned int freq_freeze_state;
	unsigned int freq_substr_window;
	unsigned int unified_byte_num;
	unsigned int filter_size;

	int printing_option;
	int verbose_option;
	int cache_value;
	std::string rev_darts_filename;
	std::string rev_dic_filename;
	std::string rev_pos_filename;

	std::vector<std::string> unk_pos_string;
	std::vector<unsigned short> unk_pos;
	std::vector<unsigned short> unk_figure_pos;

	int decay_gap;
	int reflush_period;

	Parameter(const std::string &in_master_path,
			const std::string &in_dic_header,
			const std::string &in_ftmpl_filename,
			const std::string &in_subftmpl_filename,
			const unsigned int in_iteration_num,
			const bool in_unknown_word_detection,
			const bool in_shuffle_training_data,
			const unsigned int in_unk_max_length, const bool in_debug,
			const unsigned int in_division, const unsigned int in_N,
			const unsigned int in_freeze, const unsigned int in_fsw,
			const unsigned int in_ubn, const unsigned int in_cm,
			const unsigned int in_filter, const int in_po,
			const unsigned int in_gs, const unsigned int in_thres,
			const int in_vo, const int in_cv, const int in_gap, const int in_reflush) {

		master_path = in_master_path;

		ftmpl_filename = in_master_path + "/" + in_ftmpl_filename;
		subftmpl_filename = in_master_path + "/" + in_subftmpl_filename;

		iteration_num = in_iteration_num;
		unknown_word_detection = in_unknown_word_detection;
		shuffle_training_data = in_shuffle_training_data;
		unk_max_length = in_unk_max_length;
		debug = in_debug;
		division = in_division;
		N = in_N;
		freq_freeze_state = in_freeze;
		freq_substr_window = in_fsw;
		unified_byte_num = in_ubn;
		config_mode = in_cm;
		filter_size = in_filter;
		printing_option = in_po;
		generic_switch = in_gs;
		freq_threshold = in_thres;
		verbose_option = in_vo;
		cache_value = in_cv;
		decay_gap = in_gap;
		reflush_period = in_reflush;

		N_redundant = N + (unsigned int)(ceil(N * B_BEST_REDUNDANCY));

		darts_filename = in_master_path + "/" + in_dic_header + ".da";
		dic_filename = in_master_path + "/" + in_dic_header + ".bin";
		pos_filename = in_master_path + "/" + in_dic_header + ".pos";

		rev_darts_filename = in_master_path + "/rev." + in_dic_header + ".da";
		rev_dic_filename = in_master_path + "/rev." + in_dic_header + ".bin";
		rev_pos_filename = in_master_path + "/rev." + in_dic_header + ".pos";
	}

};

}

#endif
