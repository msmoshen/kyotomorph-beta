#include "common.h"
#include "pos.h"
#include "sentence.h"
#include "feature.h"

namespace Morph {

FeatureSet::FeatureSet(FeatureTemplateSet *in_ftmpl) {
	ftmpl = in_ftmpl;
}

FeatureSet::~FeatureSet() {
	fset.clear();
}

void FeatureSet::extract_maxsub_begin_unigram_feature(Node* node,
		Morph::BoundAttrs* boundAttrs) {

	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_template_type() != 0) // skip bigram feature template
			continue;
		std::vector<unsigned int>::iterator type_checker =
				(*tmpl_it)->get_features()->begin();
		if (*type_checker != FEATURE_MACRO_HIT_BEGIN)
			continue;

		bool informative_hit = false;
		bool redundant_hit = false;

		std::string f_head = (*tmpl_it)->get_name() + ":";
		std::string f;

		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			if (*it == FEATURE_MACRO_HIT_BEGIN)
				f += "1";
			else if (*it == FEATURE_MACRO_MS_FREQ) {
//				int freq = boundAttrs->freqEntityRef->freq;
//				if (freq < 6)
//					f += int2string(freq);
//				else if (freq < 10)
//					f += "mid";
//				else
//					f += "high";

				std::string freq_tag = boundAttrs->freq_tag;
				f += freq_tag;

			} else if (*it == FEATURE_MACRO_MS_LENGTH) {
				f += int2string(boundAttrs->freqEntityRef->strLength);
			}

//			else if (*it == FEATURE_MACRO_MS_AORB) {
//				bool is_a = false;
//				bool is_b = false;
//				if (boundAttrs->dic_attributes.at(1)
//						|| boundAttrs->dic_attributes.at(6)
//						|| boundAttrs->dic_attributes.at(7)) {
//					is_a = true;
//				}
//				if (boundAttrs->dic_attributes.at(2)
//						|| boundAttrs->dic_attributes.at(3)
//						|| boundAttrs->dic_attributes.at(4)) {
//					is_b = true;
//				}
//				if (is_a != is_b) {
//					if (is_a) {
//						f += "1";
//					} else {
//						f += "-1";
//					}
//				} else {
//					if (is_a) {
//						f += "2";
//					} else {
//						f += "0";
//					}
//				}
//			}

			else if (*it == FEATURE_MACRO_MS_INTERNAL) {
				if (boundAttrs->dic_attributes.at(5)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_BOUND_JUSTIFY) {
				if (boundAttrs->dic_attributes.at(1)
						|| boundAttrs->dic_attributes.at(6)
						|| boundAttrs->dic_attributes.at(7)
						|| boundAttrs->dic_attributes.at(8)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_BOUND_MULTIJUSTIFY) {
				if (boundAttrs->dic_attributes.at(1)
						&& (boundAttrs->dic_attributes.at(6)
								|| boundAttrs->dic_attributes.at(7)
								|| boundAttrs->dic_attributes.at(8))) {
					f += "1";
				} else {
					f += "0";
				}
			}

			//			else if (*it == FEATURE_MACRO_PERFECT_MATCH) {
//				if (boundAttrs->dic_attributes.at(7)) {
//					f += "1";
//				} else {
//					f += "0";
//				}
//			}

			else if (*it == FEATURE_MACRO_MS_COMPETETION_CROSS) {
				if (boundAttrs->dic_attributes.at(2)
						&& (boundAttrs->dic_attributes.at(6)
								|| boundAttrs->dic_attributes.at(7)
								|| boundAttrs->dic_attributes.at(8))) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_MS_COMPETETION_MULTICROSS) {
				if (boundAttrs->dic_attributes.at(3)
						&& (boundAttrs->dic_attributes.at(6)
								|| boundAttrs->dic_attributes.at(7)
								|| boundAttrs->dic_attributes.at(8))) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_CROSS_OVER) {
				if (boundAttrs->dic_attributes.at(2)
						|| boundAttrs->dic_attributes.at(3)
						|| boundAttrs->dic_attributes.at(4)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_MS_INNER_CONNECTIVITY) {
				if (boundAttrs->dic_attributes.at(7)) {
					f += "2";
				} else if (boundAttrs->dic_attributes.at(6)
						|| boundAttrs->dic_attributes.at(1)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_MS_CROSS_CONNECTIVITY) {
				if (boundAttrs->dic_attributes.at(4)) {
					f += "2";
				} else if (boundAttrs->dic_attributes.at(3)
						|| boundAttrs->dic_attributes.at(2)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_A1) {
				if (boundAttrs->dic_attributes.at(1)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A2) {
				if (boundAttrs->dic_attributes.at(2)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A3) {
				if (boundAttrs->dic_attributes.at(3)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A4) {
				if (boundAttrs->dic_attributes.at(4)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A5) {
				if (boundAttrs->dic_attributes.at(5)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A6) {
				if (boundAttrs->dic_attributes.at(6)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A7) {
				if (boundAttrs->dic_attributes.at(7)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A8) {
				if (boundAttrs->dic_attributes.at(8)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A9) {
				if (boundAttrs->dic_attributes.at(9)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A10) {
				if (boundAttrs->dic_attributes.at(10)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A11) {
				if (boundAttrs->dic_attributes.at(11)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A12) {
				if (boundAttrs->dic_attributes.at(12)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A13) {
				if (boundAttrs->dic_attributes.at(13)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A14) {
				if (boundAttrs->dic_attributes.at(14)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A15) {
				if (boundAttrs->dic_attributes.at(15)) {
					f += "1";
				} else {
					f += "0";
				}
			}

		}
//		if(!co_not_hit)

		std::vector<std::string> feature_values;
		split_string(f, ",", feature_values);

		for (std::vector<std::string>::const_iterator f_it =
				feature_values.begin(); f_it != feature_values.end(); ++f_it) {
			if (f_it != feature_values.begin()) {
				if (*f_it == "0") {
					redundant_hit = true;
				}
				if (*f_it == "1") {
					informative_hit = true;
				}
			}
		}

		if (informative_hit || !redundant_hit) {
			f_head += f;
			fset.push_back(f_head);
		}

	}
}

void FeatureSet::extract_maxsub_end_unigram_feature(Node* node,
		Morph::BoundAttrs* boundAttrs) {

	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
		if ((*tmpl_it)->get_template_type() != 0) // skip bigram feature template
			continue;
		std::vector<unsigned int>::iterator type_checker =
				(*tmpl_it)->get_features()->begin();
		if (*type_checker != FEATURE_MACRO_HIT_END)
			continue;

		bool informative_hit = false;
		bool redundant_hit = false;

		std::string f_head = (*tmpl_it)->get_name() + ":";
		std::string f;

		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";
			if (*it == FEATURE_MACRO_HIT_END)
				f += "1";
			else if (*it == FEATURE_MACRO_MS_FREQ) {
//				int freq = boundAttrs->freqEntityRef->freq;
//				if (freq < 6)
//					f += int2string(freq);
//				else if (freq < 10)
//					f += "mid";
//				else
//					f += "high";

				std::string freq_tag = boundAttrs->freq_tag;
				f += freq_tag;

			} else if (*it == FEATURE_MACRO_MS_LENGTH) {
				f += int2string(boundAttrs->freqEntityRef->strLength);
			}

//			else if (*it == FEATURE_MACRO_MS_AORB) {
//				bool is_a = false;
//				bool is_b = false;
//				if (boundAttrs->dic_attributes.at(13)
//						|| boundAttrs->dic_attributes.at(10)
//						|| boundAttrs->dic_attributes.at(7)) {
//					is_a = true;
//				}
//				if (boundAttrs->dic_attributes.at(12)
//						|| boundAttrs->dic_attributes.at(11)
//						|| boundAttrs->dic_attributes.at(8)) {
//					is_b = true;
//				}
//
//				if (is_a != is_b) {
//					if (is_a) {
//						f += "1";
//					} else {
//						f += "-1";
//					}
//				} else {
//					if (is_a) {
//						f += "2";
//					} else {
//						f += "0";
//					}
//				}
//			}

			else if (*it == FEATURE_MACRO_MS_INTERNAL) {
				if (boundAttrs->dic_attributes.at(5)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_BOUND_JUSTIFY) {
				if (boundAttrs->dic_attributes.at(13)
						|| boundAttrs->dic_attributes.at(10)
						|| boundAttrs->dic_attributes.at(7)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_BOUND_MULTIJUSTIFY) {
				if (boundAttrs->dic_attributes.at(13)
						&& (boundAttrs->dic_attributes.at(10)
								|| boundAttrs->dic_attributes.at(7))) {
					f += "1";
				} else {
					f += "0";
				}
			}

//			else if (*it == FEATURE_MACRO_PERFECT_MATCH) {
//				if (boundAttrs->dic_attributes.at(7)) {
//					f += "1";
//				} else {
//					f += "0";
//				}
//			}

			else if (*it == FEATURE_MACRO_MS_COMPETETION_CROSS) {
				if (boundAttrs->dic_attributes.at(12)
						&& (boundAttrs->dic_attributes.at(4)
								|| boundAttrs->dic_attributes.at(7)
								|| boundAttrs->dic_attributes.at(10))) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_MS_COMPETETION_MULTICROSS) {
				if (boundAttrs->dic_attributes.at(11)
						&& (boundAttrs->dic_attributes.at(4)
								|| boundAttrs->dic_attributes.at(7)
								|| boundAttrs->dic_attributes.at(10))) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_CROSS_OVER) {
				if (boundAttrs->dic_attributes.at(12)
						|| boundAttrs->dic_attributes.at(11)
						|| boundAttrs->dic_attributes.at(8)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_MS_INNER_CONNECTIVITY) {
				if (boundAttrs->dic_attributes.at(7)) {
					f += "2";
				} else if (boundAttrs->dic_attributes.at(10)
						|| boundAttrs->dic_attributes.at(13)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_MS_CROSS_CONNECTIVITY) {
				if (boundAttrs->dic_attributes.at(8)) {
					f += "2";
				} else if (boundAttrs->dic_attributes.at(12)
						|| boundAttrs->dic_attributes.at(11)) {
					f += "1";
				} else {
					f += "0";
				}
			}

			else if (*it == FEATURE_MACRO_A1) {
				if (boundAttrs->dic_attributes.at(1)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A2) {
				if (boundAttrs->dic_attributes.at(2)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A3) {
				if (boundAttrs->dic_attributes.at(3)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A4) {
				if (boundAttrs->dic_attributes.at(4)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A5) {
				if (boundAttrs->dic_attributes.at(5)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A6) {
				if (boundAttrs->dic_attributes.at(6)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A7) {
				if (boundAttrs->dic_attributes.at(7)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A8) {
				if (boundAttrs->dic_attributes.at(8)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A9) {
				if (boundAttrs->dic_attributes.at(9)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A10) {
				if (boundAttrs->dic_attributes.at(10)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A11) {
				if (boundAttrs->dic_attributes.at(11)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A12) {
				if (boundAttrs->dic_attributes.at(12)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A13) {
				if (boundAttrs->dic_attributes.at(13)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A14) {
				if (boundAttrs->dic_attributes.at(14)) {
					f += "1";
				} else {
					f += "0";
				}
			} else if (*it == FEATURE_MACRO_A15) {
				if (boundAttrs->dic_attributes.at(15)) {
					f += "1";
				} else {
					f += "0";
				}
			}

		}
//		if(!co_not_hit)

		std::vector<std::string> feature_values;
		split_string(f, ",", feature_values);

		for (std::vector<std::string>::const_iterator f_it =
				feature_values.begin(); f_it != feature_values.end(); ++f_it) {
			if (f_it != feature_values.begin()) {
				if (*f_it == "0") {
					redundant_hit = true;
				}
				if (*f_it == "1") {
					informative_hit = true;
				}
			}
		}

		if (informative_hit || !redundant_hit) {
			f_head += f;
			fset.push_back(f_head);
		}

	}
}

//void FeatureSet::extract_unigram_feature(Node *node) {
//	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
//			ftmpl->get_templates()->begin();
//			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
//
//		if ((*tmpl_it)->get_template_type() != 0) // skip bigram feature template
//			continue;
//		std::string f = (*tmpl_it)->get_name() + ":";
//		for (std::vector<unsigned int>::iterator it =
//				(*tmpl_it)->get_features()->begin();
//				it != (*tmpl_it)->get_features()->end(); it++) {
//			if (it != (*tmpl_it)->get_features()->begin())
//				f += ",";
//			if (*it == FEATURE_MACRO_WORD)
//				f += *(node->string);
//			else if (*it == FEATURE_MACRO_POS)
//				f += *(node->pos);
//			else if (*it == FEATURE_MACRO_LENGTH)
//				f += int2string(node->get_char_num());
//			else if (*it == FEATURE_MACRO_BEGINNING_CHAR)
//				f.append(node->get_first_char(),
//						(node->stat & MORPH_PSEUDO_NODE) ?
//								strlen(node->get_first_char()) :
//								utf8_bytes(
//										(unsigned char *) node->get_first_char()));
//			else if (*it == FEATURE_MACRO_ENDING_CHAR) {
//				f += *(node->end_string);
//				// cerr << *(node->string) << " : " << *(node->string_for_print) << " : " << f << endl;
//			} else if (*it == FEATURE_MACRO_BEGINNING_CHAR_TYPE)
//				f += int2string(node->char_family);
//			else if (*it == FEATURE_MACRO_ENDING_CHAR_TYPE)
//				f += int2string(node->end_char_family);
//			else if (*it == FEATURE_MACRO_FEATURE1) // Wikipedia (test)
//				f += int2string(node->lcAttr);
//		}
//		fset.push_back(f);
//	}
//}
//
//void FeatureSet::extract_bigram_feature(Node *l_node, Node *r_node) {
//	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
//			ftmpl->get_templates()->begin();
//			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {
//
//		if ((*tmpl_it)->get_template_type() != 1) // skip unigram feature template
//			continue;
//		std::string f = (*tmpl_it)->get_name() + ":";
//		for (std::vector<unsigned int>::iterator it =
//				(*tmpl_it)->get_features()->begin();
//				it != (*tmpl_it)->get_features()->end(); it++) {
//			if (it != (*tmpl_it)->get_features()->begin())
//				f += ",";
//			// left
//			if (*it == FEATURE_MACRO_LEFT_WORD)
//				f += *(l_node->string);
//			else if (*it == FEATURE_MACRO_LEFT_POS)
//				f += *(l_node->pos);
//			else if (*it == FEATURE_MACRO_LEFT_LENGTH)
//				f += int2string(l_node->get_char_num());
//			else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR)
//				f.append(l_node->get_first_char(),
//						(l_node->stat & MORPH_PSEUDO_NODE) ?
//								strlen(l_node->get_first_char()) :
//								utf8_bytes(
//										(unsigned char *) l_node->get_first_char()));
//			else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR) {
//				f += *(l_node->end_string);
//				// cerr << *(l_node->string) << " : " << *(l_node->string_for_print) << " : " << f << endl;
//			} else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE)
//				f += int2string(l_node->char_family);
//			else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE)
//				f += int2string(l_node->end_char_family);
//			// right
//			else if (*it == FEATURE_MACRO_RIGHT_WORD)
//				f += *(r_node->string);
//			else if (*it == FEATURE_MACRO_RIGHT_POS)
//				f += *(r_node->pos);
//			else if (*it == FEATURE_MACRO_RIGHT_LENGTH)
//				f += int2string(r_node->get_char_num());
//			else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR)
//				f.append(r_node->get_first_char(),
//						(r_node->stat & MORPH_PSEUDO_NODE) ?
//								strlen(r_node->get_first_char()) :
//								utf8_bytes(
//										(unsigned char *) r_node->get_first_char()));
//			else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR)
//				f += *(r_node->end_string);
//			else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE)
//				f += int2string(r_node->char_family);
//			else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE)
//				f += int2string(r_node->end_char_family);
//		}
//		fset.push_back(f);
//	}
//}

//

void FeatureSet::extract_unigram_feature(Node *node) {
	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {

		if ((*tmpl_it)->get_template_type() != 0) // skip bigram feature template
			continue;

		bool redundant_hit = false;

		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";

			if (node->tagid == MORPH_WORD_TAG) {

				if (*it == FEATURE_MACRO_WORD) {
					if (node->tagid == MORPH_WORD_TAG)
						f += *(node->string);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_POS) {
					if (node->tagid >= MORPH_SC_TAG) {
						f += *(node->pos);
						f += "_";
						f += char_node_type_table[node->tagid];
					} else {
						f += *(node->pos);
					}
				}
				else if (*it == FEATURE_MACRO_LENGTH) {
					if (node->tagid == MORPH_WORD_TAG)
						f += int2string(node->get_char_num());
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_BEGINNING_CHAR) {
					if (node->tagid == MORPH_WORD_TAG && node->get_char_num() > 1)
						f.append(node->get_first_char(),
								(node->stat & MORPH_UNK_NODE) ?
										strlen(node->get_first_char()) :
										utf8_bytes(
												(unsigned char *) node->get_first_char()));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_ENDING_CHAR) {
					if (node->tagid == MORPH_WORD_TAG && node->get_char_num() > 1)
						f += *(node->end_string);
					else
						redundant_hit = true;
					// cerr << *(node->string) << " : " << *(node->string_for_print) << " : " << f << endl;
				}
				else if (*it == FEATURE_MACRO_BEGINNING_CHAR_TYPE) {
					if (node->tagid == MORPH_WORD_TAG && node->get_char_num() > 1)
						f += int2string(node->char_family);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_ENDING_CHAR_TYPE) {
					if (node->tagid == MORPH_WORD_TAG && node->get_char_num() > 1)
						f += int2string(node->end_char_family);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_SINGLE_CHAR) {
					if (node->tagid == MORPH_WORD_TAG && node->get_char_num() == 1)
						f += *(node->string);
					else
						redundant_hit = true;
	//				 cerr << *(node->string) << " : " << *(node->string_for_print) << " : " << f << endl;
				}
				else if (*it == FEATURE_MACRO_SINGLE_CHAR_TYPE) {
					if (node->tagid == MORPH_WORD_TAG && node->get_char_num() == 1)
						f += int2string(node->char_family);
					else
						redundant_hit = true;
				}
				else
					redundant_hit = true;

			} else {

				if (*it == FEATURE_MACRO_TAG) {
					f += char_node_type_table[node->tagid];
				}
				else if (*it == FEATURE_MACRO_POS) {
					if (node->tagid >= MORPH_SC_TAG) {
						f += *(node->pos);
						f += "_";
						f += char_node_type_table[node->tagid];
					} else {
						f += *(node->pos);
					}
				}
				else if (*it == FEATURE_MACRO_m2_CHAR) {
					if (node->tagid >= MORPH_SC_TAG)
						f += *(node->l2_char);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_m1_CHAR) {
					if (node->tagid >= MORPH_SC_TAG)
						f += *(node->l_char);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_CHAR) {
					if (node->tagid >= MORPH_SC_TAG)
						f += *(node->string_for_print);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_p1_CHAR) {
					if (node->tagid >= MORPH_SC_TAG)
						f += *(node->r_char);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_p2_CHAR) {
					if (node->tagid >= MORPH_SC_TAG)
						f += *(node->r2_char);
					else
						redundant_hit = true;
				}

				else if (*it == FEATURE_MACRO_m2_CHAR_TYPE) {
					if (node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) node->l2_char->c_str())));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_m1_CHAR_TYPE) {
					if (node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) node->l_char->c_str())));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_CHAR_TYPE) {
					if (node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) node->string_for_print->c_str())));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_p1_CHAR_TYPE) {
					if (node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) node->r_char->c_str())));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_p2_CHAR_TYPE) {
					if (node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) node->r2_char->c_str())));
					else
						redundant_hit = true;
				}
				else
					redundant_hit = true;

			}

		}

		if (!redundant_hit) {
//			cerr << "feature uni: " << f << endl;
			fset.push_back(f);
		}

	}
}

void FeatureSet::extract_bigram_feature(Node *l_node, Node *r_node) {
	for (std::vector<FeatureTemplate *>::iterator tmpl_it =
			ftmpl->get_templates()->begin();
			tmpl_it != ftmpl->get_templates()->end(); tmpl_it++) {

		if ((*tmpl_it)->get_template_type() != 1) // skip unigram feature template
			continue;

		bool redundant_hit = false;

		std::string f = (*tmpl_it)->get_name() + ":";
		for (std::vector<unsigned int>::iterator it =
				(*tmpl_it)->get_features()->begin();
				it != (*tmpl_it)->get_features()->end(); it++) {
			if (it != (*tmpl_it)->get_features()->begin())
				f += ",";

			if (l_node->tagid == MORPH_WORD_TAG && r_node->tagid == MORPH_WORD_TAG) {
				// left
				if (*it == FEATURE_MACRO_LEFT_WORD)
					f += *(l_node->string);
				else if (*it == FEATURE_MACRO_LEFT_POS)
					f += *(l_node->pos);
				else if (*it == FEATURE_MACRO_LEFT_LENGTH)
					f += int2string(l_node->get_char_num());
				else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR) {
					if (l_node->stat == MORPH_BOS_NODE || l_node->stat == MORPH_EOS_NODE)
						f += *(l_node->string);
					else
						f.append(l_node->get_first_char(),
							(l_node->stat & MORPH_UNK_NODE) ?
									strlen(l_node->get_first_char()) :
									utf8_bytes(
											(unsigned char *) l_node->get_first_char()));
				}
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR) {
					if (l_node->stat == MORPH_BOS_NODE || l_node->stat == MORPH_EOS_NODE)
						f += *(l_node->string);
					else
						f += *(l_node->end_string);
					// cerr << *(l_node->string) << " : " << *(l_node->string_for_print) << " : " << f << endl;
				}
				else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE)
					f += int2string(l_node->char_family);
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE)
					f += int2string(l_node->end_char_family);
				// right
				else if (*it == FEATURE_MACRO_RIGHT_WORD)
					f += *(r_node->string);
				else if (*it == FEATURE_MACRO_RIGHT_POS)
					f += *(r_node->pos);
				else if (*it == FEATURE_MACRO_RIGHT_LENGTH)
					f += int2string(r_node->get_char_num());
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR) {
					if (r_node->stat == MORPH_BOS_NODE || r_node->stat == MORPH_EOS_NODE)
						f += *(r_node->string);
					else
						f.append(r_node->get_first_char(),
							(r_node->stat & MORPH_UNK_NODE) ?
									strlen(r_node->get_first_char()) :
									utf8_bytes(
											(unsigned char *) r_node->get_first_char()));
				}
				else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR) {
					if (r_node->stat == MORPH_BOS_NODE || r_node->stat == MORPH_EOS_NODE)
						f += *(r_node->string);
					else
						f += *(r_node->end_string);
				}
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE)
					f += int2string(r_node->char_family);
				else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE)
					f += int2string(r_node->end_char_family);
				else
					redundant_hit = true;

			} else if (l_node->tagid >= MORPH_SC_TAG && r_node->tagid >= MORPH_SC_TAG) {
				//left
				if (*it == FEATURE_MACRO_LEFT_TAG) {
					f += char_node_type_table[l_node->tagid];
				}
				else if (*it == FEATURE_MACRO_LEFT_POS) {
					if (l_node->tagid >= MORPH_SC_TAG) {
						f += *(l_node->pos);
						f += "_";
						f += char_node_type_table[l_node->tagid];
					} else {
						f += *(l_node->pos);
					}
				}
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR) {
					if (l_node->tagid == MORPH_SC_TAG || l_node->tagid == MORPH_EC_TAG)
						f += *(l_node->string_for_print);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE) {
					if (l_node->tagid == MORPH_SC_TAG || l_node->tagid == MORPH_EC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) l_node->string_for_print->c_str())));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_LEFT_CHAR) {
					if (l_node->tagid == MORPH_SC_TAG || l_node->tagid == MORPH_EC_TAG)
						redundant_hit = true;
					else
						f += *(l_node->string_for_print);
				}
				else if (*it == FEATURE_MACRO_LEFT_CHAR_TYPE) {
					if (l_node->tagid == MORPH_SC_TAG || l_node->tagid == MORPH_EC_TAG)
						redundant_hit = true;
					else
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) l_node->string_for_print->c_str())));
				}
				//right
				else if (*it == FEATURE_MACRO_RIGHT_TAG) {
					f += char_node_type_table[r_node->tagid];
				}
				else if (*it == FEATURE_MACRO_RIGHT_POS) {
					if (r_node->tagid >= MORPH_SC_TAG) {
						f += *(r_node->pos);
						f += "_";
						f += char_node_type_table[r_node->tagid];
					} else {
						f += *(r_node->pos);
					}
				}
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR) {
					if (r_node->tagid == MORPH_SC_TAG || r_node->tagid == MORPH_BC_TAG)
						f += *(r_node->string_for_print);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE) {
					if (r_node->tagid == MORPH_SC_TAG || r_node->tagid == MORPH_BC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) r_node->string_for_print->c_str())));
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_RIGHT_CHAR) {
					if (r_node->tagid == MORPH_SC_TAG || r_node->tagid == MORPH_BC_TAG)
						redundant_hit = true;
					else
						f += *(r_node->string_for_print);
				}
				else if (*it == FEATURE_MACRO_RIGHT_CHAR_TYPE) {
					if (r_node->tagid == MORPH_SC_TAG || r_node->tagid == MORPH_BC_TAG)
						redundant_hit = true;
					else
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) r_node->string_for_print->c_str())));
				}
				else
					redundant_hit = true;

			} else if (l_node->tagid == MORPH_WORD_TAG && r_node->tagid >= MORPH_SC_TAG) {
				//left word nodes
				if (*it == FEATURE_MACRO_LEFT_WORD)
					f += *(l_node->string);
				else if (*it == FEATURE_MACRO_LEFT_POS)
					f += *(l_node->pos);
				else if (*it == FEATURE_MACRO_LEFT_LENGTH)
					f += int2string(l_node->get_char_num());
				else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR) {
					if (l_node->stat == MORPH_BOS_NODE || l_node->stat == MORPH_EOS_NODE)
						f += *(l_node->string);
					else
						f.append(l_node->get_first_char(),
							(l_node->stat & MORPH_UNK_NODE) ?
									strlen(l_node->get_first_char()) :
									utf8_bytes(
											(unsigned char *) l_node->get_first_char()));
				}
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR) {
					if (l_node->stat == MORPH_BOS_NODE || l_node->stat == MORPH_EOS_NODE)
						f += *(l_node->string);
					else
						f += *(l_node->end_string);
					// cerr << *(l_node->string) << " : " << *(l_node->string_for_print) << " : " << f << endl;
				}
				else if (*it == FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE)
					f += int2string(l_node->char_family);
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE)
					f += int2string(l_node->end_char_family);
				//right char nodes
				else if (*it == FEATURE_MACRO_RIGHT_TAG) {
					f += char_node_type_table[r_node->tagid];
				}
				else if (*it == FEATURE_MACRO_RIGHT_POS) {
					if (r_node->tagid >= MORPH_SC_TAG) {
						f += *(r_node->pos);
						f += "_";
						f += char_node_type_table[r_node->tagid];
					} else {
						f += *(r_node->pos);
					}
				}
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR) {
					if (r_node->tagid >= MORPH_SC_TAG)
						f += *(r_node->string_for_print);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE) {
					if (r_node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) r_node->string_for_print->c_str())));
					else
						redundant_hit = true;
				}
				else
					redundant_hit = true;

			} else if (l_node->tagid >= MORPH_SC_TAG && r_node->tagid == MORPH_WORD_TAG) {
				//left char nodes
				if (*it == FEATURE_MACRO_LEFT_TAG) {
					f += char_node_type_table[l_node->tagid];
				}
				else if (*it == FEATURE_MACRO_LEFT_POS) {
					if (l_node->tagid >= MORPH_SC_TAG) {
						f += *(l_node->pos);
						f += "_";
						f += char_node_type_table[l_node->tagid];
					} else {
						f += *(l_node->pos);
					}
				}
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR) {
					if (l_node->tagid >= MORPH_SC_TAG)
						f += *(l_node->string_for_print);
					else
						redundant_hit = true;
				}
				else if (*it == FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE) {
					if (l_node->tagid >= MORPH_SC_TAG)
						f += int2string(check_char_family(check_utf8_char_type((unsigned char *) l_node->string_for_print->c_str())));
					else
						redundant_hit = true;
				}
				// right word nodes
				else if (*it == FEATURE_MACRO_RIGHT_WORD)
					f += *(r_node->string);
				else if (*it == FEATURE_MACRO_RIGHT_POS)
					f += *(r_node->pos);
				else if (*it == FEATURE_MACRO_RIGHT_LENGTH)
					f += int2string(r_node->get_char_num());
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR) {
					if (r_node->stat == MORPH_BOS_NODE || r_node->stat == MORPH_EOS_NODE)
						f += *(r_node->string);
					else
						f.append(r_node->get_first_char(),
							(r_node->stat & MORPH_UNK_NODE) ?
									strlen(r_node->get_first_char()) :
									utf8_bytes(
											(unsigned char *) r_node->get_first_char()));
				}
				else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR) {
					if (r_node->stat == MORPH_BOS_NODE || r_node->stat == MORPH_EOS_NODE)
						f += *(r_node->string);
					else
						f += *(r_node->end_string);
				}
				else if (*it == FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE)
					f += int2string(r_node->char_family);
				else if (*it == FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE)
					f += int2string(r_node->end_char_family);
				else
					redundant_hit = true;

			}

		}

		if (!redundant_hit) {
//		cerr << "feature bi: " << f << endl;
			fset.push_back(f);
		}

	}
}

//

bool FeatureSet::append_feature(FeatureSet *in) {
	for (std::vector<std::string>::iterator it = in->fset.begin();
			it != in->fset.end(); it++) {
		fset.push_back(*it);
	}
	return true;
}

int FeatureSet::calc_inner_product_with_weight() {
	double sum = 0;
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		if (feature_weight.find(*it) != feature_weight.end()) {
			sum += feature_weight[*it];
		}
	}
	return sum;
}

void FeatureSet::minus_feature_from_weight(
		std::map<std::string, double> &in_feature_weight, int factor) {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		in_feature_weight[*it] -= factor;

//		cerr << "minus feature: " << *it << "\t" << in_feature_weight[*it] << endl;
	}
}

void FeatureSet::minus_feature_from_weight(
		std::map<std::string, double> &in_feature_weight) {
	minus_feature_from_weight(in_feature_weight, 1);
}

void FeatureSet::plus_feature_from_weight(
		std::map<std::string, double> &in_feature_weight, int factor) {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		in_feature_weight[*it] += factor;

//		cerr << "plus feature: " << *it << "\t" << in_feature_weight[*it] << endl;
	}
}

void FeatureSet::plus_feature_from_weight(
		std::map<std::string, double> &in_feature_weight) {
	plus_feature_from_weight(in_feature_weight, 1);
}

void FeatureSet::minus_feature_from_weight(
		std::map<std::string, double> &in_feature_weight,
		std::map<std::string, int> &in_feature_count, int factor) {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {

		in_feature_weight[*it] -= factor;

//		if (*it == "BxFrAb:1,4,1") {
//			cerr << *it << ": -1  now: " << in_feature_weight[*it] << endl;
//		}

	}
}

void FeatureSet::plus_feature_from_weight(
		std::map<std::string, double> &in_feature_weight,
		std::map<std::string, int> &in_feature_count, int factor) {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {

		in_feature_weight[*it] += factor;

//		if (*it == "BxFrAb:1,4,1") {
//			cerr << *it << ": +1  now: " << in_feature_weight[*it] << endl;
//		}

	}
}

bool FeatureSet::print() {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		cout << *it << " ";
	}
	cout << endl;
	return true;
}

void FeatureSet::gather_feature(std::map<std::string, int>& f_collection) {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {

		if (f_collection.find(*it) != f_collection.end()) {
			f_collection[*it] += 1;
		} else {
			f_collection[*it] = 1;
		}

	}
}

void FeatureSet::diff_feature(std::map<std::string, int>& f_collection,
		bool is_good) {
	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {

		if (f_collection.find(*it) != f_collection.end()) {
			if (is_good)
				f_collection[*it] += 1;
			else
				f_collection[*it] -= 1;
		} else {
			if (is_good)
				f_collection[*it] = 1;
			else
				f_collection[*it] = -1;
		}

	}
}

bool FeatureSet::print_err() {
//	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
//			it++) {
//		if (*it == "BxFrAb:1,4,1") {
//			cerr << *it << " ";
//			return true;
//		}
//	}
//	return false;

	for (std::vector<std::string>::iterator it = fset.begin(); it != fset.end();
			it++) {
		cerr << *it << " ";
	}
	cerr << endl;
	return true;
}

FeatureTemplate::FeatureTemplate(std::string &in_name,
		std::string &feature_string, int in_template_type) {
	template_type = in_template_type;
	name = in_name;
	std::vector<std::string> line;
	split_string(feature_string, ",", line);
	for (std::vector<std::string>::iterator it = line.begin(); it != line.end();
			it++) {
		unsigned int macro_id = interpret_macro(*it);
		if (macro_id)
			features.push_back(macro_id);
	}
}

unsigned int FeatureTemplate::interpret_macro(std::string &macro) {
	// unigram
	if (template_type == 0) {
		if (macro == FEATURE_MACRO_STRING_WORD)
			return FEATURE_MACRO_WORD;
		else if (macro == FEATURE_MACRO_STRING_POS)
			return FEATURE_MACRO_POS;
		else if (macro == FEATURE_MACRO_STRING_LENGTH)
			return FEATURE_MACRO_LENGTH;
		else if (macro == FEATURE_MACRO_STRING_BEGINNING_CHAR)
			return FEATURE_MACRO_BEGINNING_CHAR;
		else if (macro == FEATURE_MACRO_STRING_ENDING_CHAR)
			return FEATURE_MACRO_ENDING_CHAR;
		else if (macro == FEATURE_MACRO_STRING_BEGINNING_CHAR_TYPE)
			return FEATURE_MACRO_BEGINNING_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_ENDING_CHAR_TYPE)
			return FEATURE_MACRO_ENDING_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_SINGLE_CHAR)
			return FEATURE_MACRO_SINGLE_CHAR;
		else if (macro == FEATURE_MACRO_STRING_SINGLE_CHAR_TYPE)
			return FEATURE_MACRO_SINGLE_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_FEATURE1)
//			return FEATURE_MACRO_FEATURE1;
		else if (macro == FEATURE_MACRO_STRING_HIT_BEGIN)
			return FEATURE_MACRO_HIT_BEGIN;
		else if (macro == FEATURE_MACRO_STRING_HIT_END)
			return FEATURE_MACRO_HIT_END;
		else if (macro == FEATURE_MACRO_STRING_MS_FREQ)
			return FEATURE_MACRO_MS_FREQ;
		else if (macro == FEATURE_MACRO_STRING_MS_LENGTH)
			return FEATURE_MACRO_MS_LENGTH;
		else if (macro == FEATURE_MACRO_STRING_MS_GREATER)
			return FEATURE_MACRO_MS_GREATER;
//		else if (macro == FEATURE_MACRO_STRING_MS_AORB)
//			return FEATURE_MACRO_MS_AORB;
		else if (macro == FEATURE_MACRO_STRING_BOUND_JUSTIFY)
			return FEATURE_MACRO_BOUND_JUSTIFY;
		else if (macro == FEATURE_MACRO_STRING_BOUND_MULTIJUSTIFY)
			return FEATURE_MACRO_BOUND_MULTIJUSTIFY;
		else if (macro == FEATURE_MACRO_STRING_CROSS_OVER)
			return FEATURE_MACRO_CROSS_OVER;
		else if (macro == FEATURE_MACRO_STRING_MS_INTERNAL)
			return FEATURE_MACRO_MS_INTERNAL;
		else if (macro == FEATURE_MACRO_STRING_MS_INNER_CONNECTIVITY)
			return FEATURE_MACRO_MS_INNER_CONNECTIVITY;
		else if (macro == FEATURE_MACRO_STRING_MS_CROSS_CONNECTIVITY)
			return FEATURE_MACRO_MS_CROSS_CONNECTIVITY;

		else if (macro == FEATURE_MACRO_STRING_MS_COMPETETION_CROSS)
			return FEATURE_MACRO_MS_COMPETETION_CROSS;
		else if (macro == FEATURE_MACRO_STRING_MS_COMPETETION_MULTICROSS)
			return FEATURE_MACRO_MS_COMPETETION_MULTICROSS;

		else if (macro == FEATURE_MACRO_STRING_A1)
			return FEATURE_MACRO_A1;
		else if (macro == FEATURE_MACRO_STRING_A2)
			return FEATURE_MACRO_A2;
		else if (macro == FEATURE_MACRO_STRING_A3)
			return FEATURE_MACRO_A3;
		else if (macro == FEATURE_MACRO_STRING_A4)
			return FEATURE_MACRO_A4;
		else if (macro == FEATURE_MACRO_STRING_A5)
			return FEATURE_MACRO_A5;
		else if (macro == FEATURE_MACRO_STRING_A6)
			return FEATURE_MACRO_A6;
		else if (macro == FEATURE_MACRO_STRING_A7)
			return FEATURE_MACRO_A7;
		else if (macro == FEATURE_MACRO_STRING_A8)
			return FEATURE_MACRO_A8;
		else if (macro == FEATURE_MACRO_STRING_A9)
			return FEATURE_MACRO_A9;
		else if (macro == FEATURE_MACRO_STRING_A10)
			return FEATURE_MACRO_A10;
		else if (macro == FEATURE_MACRO_STRING_A11)
			return FEATURE_MACRO_A11;
		else if (macro == FEATURE_MACRO_STRING_A12)
			return FEATURE_MACRO_A12;
		else if (macro == FEATURE_MACRO_STRING_A13)
			return FEATURE_MACRO_A13;
		else if (macro == FEATURE_MACRO_STRING_A14)
			return FEATURE_MACRO_A14;
		else if (macro == FEATURE_MACRO_STRING_A15)
			return FEATURE_MACRO_A15;
		else if (macro == FEATURE_MACRO_STRING_TAG)
			return FEATURE_MACRO_TAG;
//		else if (macro == FEATURE_MACRO_STRING_SOLE_CHAR_TYPE)
//			return FEATURE_MACRO_SOLE_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_m2_CHAR)
			return FEATURE_MACRO_m2_CHAR;
		else if (macro == FEATURE_MACRO_STRING_m1_CHAR)
			return FEATURE_MACRO_m1_CHAR;
		else if (macro == FEATURE_MACRO_STRING_CHAR)
			return FEATURE_MACRO_CHAR;
		else if (macro == FEATURE_MACRO_STRING_p1_CHAR)
			return FEATURE_MACRO_p1_CHAR;
		else if (macro == FEATURE_MACRO_STRING_p2_CHAR)
			return FEATURE_MACRO_p2_CHAR;

		else if (macro == FEATURE_MACRO_STRING_m2_CHAR_TYPE)
			return FEATURE_MACRO_m2_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_m1_CHAR_TYPE)
			return FEATURE_MACRO_m1_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_CHAR_TYPE)
			return FEATURE_MACRO_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_p1_CHAR_TYPE)
			return FEATURE_MACRO_p1_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_p2_CHAR_TYPE)
			return FEATURE_MACRO_p2_CHAR_TYPE;

	}
	// bigram
	else if (template_type == 1) {
		// bigram: left
		if (macro == FEATURE_MACRO_STRING_LEFT_WORD)
			return FEATURE_MACRO_LEFT_WORD;
		else if (macro == FEATURE_MACRO_STRING_LEFT_POS)
			return FEATURE_MACRO_LEFT_POS;
		else if (macro == FEATURE_MACRO_STRING_LEFT_LENGTH)
			return FEATURE_MACRO_LEFT_LENGTH;
		else if (macro == FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR)
			return FEATURE_MACRO_LEFT_BEGINNING_CHAR;
		else if (macro == FEATURE_MACRO_STRING_LEFT_ENDING_CHAR)
			return FEATURE_MACRO_LEFT_ENDING_CHAR;
		else if (macro == FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR_TYPE)
			return FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_LEFT_ENDING_CHAR_TYPE)
			return FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_LEFT_CHAR)
			return FEATURE_MACRO_LEFT_CHAR;
		else if (macro == FEATURE_MACRO_STRING_LEFT_CHAR_TYPE)
			return FEATURE_MACRO_LEFT_CHAR_TYPE;


		// bigram: right
		else if (macro == FEATURE_MACRO_STRING_RIGHT_WORD)
			return FEATURE_MACRO_RIGHT_WORD;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_POS)
			return FEATURE_MACRO_RIGHT_POS;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_LENGTH)
			return FEATURE_MACRO_RIGHT_LENGTH;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR)
			return FEATURE_MACRO_RIGHT_BEGINNING_CHAR;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR)
			return FEATURE_MACRO_RIGHT_ENDING_CHAR;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR_TYPE)
			return FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR_TYPE)
			return FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_CHAR)
			return FEATURE_MACRO_RIGHT_CHAR;
		else if (macro == FEATURE_MACRO_STRING_RIGHT_CHAR_TYPE)
			return FEATURE_MACRO_RIGHT_CHAR_TYPE;


		else if (macro == FEATURE_MACRO_STRING_LEFT_TAG)
			return FEATURE_MACRO_LEFT_TAG;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_SOLE_CHAR_TYPE)
//			return FEATURE_MACRO_LEFT_SOLE_CHAR_TYPE;
//
		else if (macro == FEATURE_MACRO_STRING_RIGHT_TAG)
			return FEATURE_MACRO_RIGHT_TAG;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_SOLE_CHAR_TYPE)
//			return FEATURE_MACRO_RIGHT_SOLE_CHAR_TYPE;
//
//		else if (macro == FEATURE_MACRO_STRING_LEFT_NEXT_CHAR)
//			return FEATURE_MACRO_LEFT_NEXT_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_NEXT_CHAR)
//			return FEATURE_MACRO_RIGHT_NEXT_CHAR;

	}
//	// tag unigram
//	else if (template_type == 2) {
//
//		if (macro == FEATURE_MACRO_STRING_WORD)
//			return FEATURE_MACRO_WORD;
//		else if (macro == FEATURE_MACRO_STRING_POS)
//			return FEATURE_MACRO_POS;
//		else if (macro == FEATURE_MACRO_STRING_LENGTH)
//			return FEATURE_MACRO_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_BEGINNING_CHAR)
//			return FEATURE_MACRO_BEGINNING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_ENDING_CHAR)
//			return FEATURE_MACRO_ENDING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_BEGINNING_CHAR_TYPE)
//			return FEATURE_MACRO_BEGINNING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_ENDING_CHAR_TYPE)
//			return FEATURE_MACRO_ENDING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_FEATURE1)
//			return FEATURE_MACRO_FEATURE1;
//		else if (macro == FEATURE_MACRO_STRING_HIT_BEGIN)
//			return FEATURE_MACRO_HIT_BEGIN;
//		else if (macro == FEATURE_MACRO_STRING_HIT_END)
//			return FEATURE_MACRO_HIT_END;
//		else if (macro == FEATURE_MACRO_STRING_MS_FREQ)
//			return FEATURE_MACRO_MS_FREQ;
//		else if (macro == FEATURE_MACRO_STRING_MS_LENGTH)
//			return FEATURE_MACRO_MS_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_MS_GREATER)
//			return FEATURE_MACRO_MS_GREATER;
////		else if (macro == FEATURE_MACRO_STRING_MS_AORB)
////			return FEATURE_MACRO_MS_AORB;
//		else if (macro == FEATURE_MACRO_STRING_BOUND_JUSTIFY)
//			return FEATURE_MACRO_BOUND_JUSTIFY;
//		else if (macro == FEATURE_MACRO_STRING_BOUND_MULTIJUSTIFY)
//			return FEATURE_MACRO_BOUND_MULTIJUSTIFY;
//		else if (macro == FEATURE_MACRO_STRING_CROSS_OVER)
//			return FEATURE_MACRO_CROSS_OVER;
//		else if (macro == FEATURE_MACRO_STRING_MS_INTERNAL)
//			return FEATURE_MACRO_MS_INTERNAL;
//		else if (macro == FEATURE_MACRO_STRING_MS_INNER_CONNECTIVITY)
//			return FEATURE_MACRO_MS_INNER_CONNECTIVITY;
//		else if (macro == FEATURE_MACRO_STRING_MS_CROSS_CONNECTIVITY)
//			return FEATURE_MACRO_MS_CROSS_CONNECTIVITY;
//
//		else if (macro == FEATURE_MACRO_STRING_MS_COMPETETION_CROSS)
//			return FEATURE_MACRO_MS_COMPETETION_CROSS;
//		else if (macro == FEATURE_MACRO_STRING_MS_COMPETETION_MULTICROSS)
//			return FEATURE_MACRO_MS_COMPETETION_MULTICROSS;
//
//		else if (macro == FEATURE_MACRO_STRING_A1)
//			return FEATURE_MACRO_A1;
//		else if (macro == FEATURE_MACRO_STRING_A2)
//			return FEATURE_MACRO_A2;
//		else if (macro == FEATURE_MACRO_STRING_A3)
//			return FEATURE_MACRO_A3;
//		else if (macro == FEATURE_MACRO_STRING_A4)
//			return FEATURE_MACRO_A4;
//		else if (macro == FEATURE_MACRO_STRING_A5)
//			return FEATURE_MACRO_A5;
//		else if (macro == FEATURE_MACRO_STRING_A6)
//			return FEATURE_MACRO_A6;
//		else if (macro == FEATURE_MACRO_STRING_A7)
//			return FEATURE_MACRO_A7;
//		else if (macro == FEATURE_MACRO_STRING_A8)
//			return FEATURE_MACRO_A8;
//		else if (macro == FEATURE_MACRO_STRING_A9)
//			return FEATURE_MACRO_A9;
//		else if (macro == FEATURE_MACRO_STRING_A10)
//			return FEATURE_MACRO_A10;
//		else if (macro == FEATURE_MACRO_STRING_A11)
//			return FEATURE_MACRO_A11;
//		else if (macro == FEATURE_MACRO_STRING_A12)
//			return FEATURE_MACRO_A12;
//		else if (macro == FEATURE_MACRO_STRING_A13)
//			return FEATURE_MACRO_A13;
//		else if (macro == FEATURE_MACRO_STRING_A14)
//			return FEATURE_MACRO_A14;
//		else if (macro == FEATURE_MACRO_STRING_A15)
//			return FEATURE_MACRO_A15;
//
//		else if (macro == FEATURE_MACRO_STRING_TAG)
//			return FEATURE_MACRO_TAG;
//		else if (macro == FEATURE_MACRO_STRING_SOLE_CHAR_TYPE)
//			return FEATURE_MACRO_SOLE_CHAR_TYPE;
//
//	}
//	// tag bigram
//	else if (template_type == 3) {
//		// bigram: left
//		if (macro == FEATURE_MACRO_STRING_LEFT_WORD)
//			return FEATURE_MACRO_LEFT_WORD;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_POS)
//			return FEATURE_MACRO_LEFT_POS;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_LENGTH)
//			return FEATURE_MACRO_LEFT_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR)
//			return FEATURE_MACRO_LEFT_BEGINNING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_ENDING_CHAR)
//			return FEATURE_MACRO_LEFT_ENDING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_BEGINNING_CHAR_TYPE)
//			return FEATURE_MACRO_LEFT_BEGINNING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_ENDING_CHAR_TYPE)
//			return FEATURE_MACRO_LEFT_ENDING_CHAR_TYPE;
//		// bigram: right
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_WORD)
//			return FEATURE_MACRO_RIGHT_WORD;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_POS)
//			return FEATURE_MACRO_RIGHT_POS;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_LENGTH)
//			return FEATURE_MACRO_RIGHT_LENGTH;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR)
//			return FEATURE_MACRO_RIGHT_BEGINNING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR)
//			return FEATURE_MACRO_RIGHT_ENDING_CHAR;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_BEGINNING_CHAR_TYPE)
//			return FEATURE_MACRO_RIGHT_BEGINNING_CHAR_TYPE;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_ENDING_CHAR_TYPE)
//			return FEATURE_MACRO_RIGHT_ENDING_CHAR_TYPE;
//
//		else if (macro == FEATURE_MACRO_STRING_LEFT_TAG)
//			return FEATURE_MACRO_LEFT_TAG;
//		else if (macro == FEATURE_MACRO_STRING_LEFT_SOLE_CHAR_TYPE)
//			return FEATURE_MACRO_LEFT_SOLE_CHAR_TYPE;
//
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_TAG)
//			return FEATURE_MACRO_RIGHT_TAG;
//		else if (macro == FEATURE_MACRO_STRING_RIGHT_SOLE_CHAR_TYPE)
//			return FEATURE_MACRO_RIGHT_SOLE_CHAR_TYPE;
//
//	}

	cerr << ";; cannot understand macro: " << macro << endl;
	return 0;
}

FeatureTemplate *FeatureTemplateSet::interpret_template(
		std::string &template_string, int in_template_type) {
	std::vector<std::string> line;
	split_string(template_string, ":", line);
	return new FeatureTemplate(line[0], line[1], in_template_type);
}

bool FeatureTemplateSet::open(const std::string &template_filename) {
	std::ifstream ft_in(template_filename.c_str(), std::ios::in);
	if (!ft_in.is_open()) {
		cerr << ";; cannot open " << template_filename << " for reading"
				<< endl;
		return false;
	}

	std::string buffer;
	while (getline(ft_in, buffer)) {
		if (buffer.at(0) == '#') // comment line
			continue;
		std::vector<std::string> line;
		split_string(buffer, " ", line);

		if (line[0] == "UNIGRAM") {
			templates.push_back(interpret_template(line[1], 0));
		} else if (line[0] == "BIGRAM") {
			templates.push_back(interpret_template(line[1], 1));
		} else if (line[0] == "TAG_UNI") {
			templates.push_back(interpret_template(line[1], 2));
		} else if (line[0] == "TAG_BI") {
			templates.push_back(interpret_template(line[1], 3));
		} else {
			cerr << ";; cannot understand: " << line[0] << endl;
		}
	}

	return true;
}

}
