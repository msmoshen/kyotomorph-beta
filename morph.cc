#include "common.h"
#include "tagger.h"
#include "cmdline.h"

bool MODE_TRAIN = false;
bool WEIGHT_AVERAGED = false;
bool MODE_CONFIG = false;
bool MODE_NBEST = false;
bool MODE_CACHE = false;
bool MODE_STRUCT = false;
bool MODE_JUMAN = false;
bool MODE_FILTER = false;
bool MODE_MAXSUB = false;
bool MODE_CLEAR = false;

bool MODE_POSFREE = false;


std::map<std::string, double> feature_weight;
std::map<std::string, double> feature_weight_sum;
std::map<std::string, int> feature_count;
std::map<int, std::map<int, std::vector<int> > > boundary_estimation_table;
int num_of_sentences_processed = 0;

std::vector<int> char_node_sequence;
std::map<int, std::string > char_node_type_table;
std::map<int, std::set<int> > char_node_front_connectivity;
std::map<int, std::set<int> > char_node_back_connectivity;

void option_proc(cmdline::parser &option, int argc, char **argv) {

	option.add<std::string>("master-path", 'p', "master folder", false, "data");

	option.add<std::string>("dics", 'd', "dic header", false, "dic+wikipedia+edr");

//	option.add<std::string>("darts", 'd', "darts filename", false,
//			"data/dic.da");
//	option.add<std::string>("bin", 'b', "dicbin filename", false,
//			"data/dic.bin");
//	option.add<std::string>("pos", 'p', "POS list filename", false,
//			"data/dic.pos");

	option.add<std::string>("model", 'm', "model filename", false,
			"data/model.dat");

	option.add<std::string>("train", 't', "training filename", false,
			"data/train.txt");

	option.add<std::string>("feature", 'f', "feature template filename", false,
			"feature.ext2m.tag.expand.def");

	option.add<std::string>("subfeature", 'F',
			"max-sub feature template filename", false,
			"feature.maxsub.def");

	option.add<std::string>("gold-standard", 'E', "gold-standard data for checking", false,
			"train.txt");

	option.add<unsigned int>("switch", 'K', "generic switching option", false,
			0);

	option.add<std::string>("file", 'L', "file name of input data", false,
			"data.cof");
	option.add<std::string>("data", 'D', "read-in generic data", false,
			"data.cof");
	option.add<std::string>("struct", 'S', "read-in structured data", false,
			"data.cof");
	option.add<std::string>("punctuation", 'P', "punctuation table", false,
			"punc.tab");

	option.add<unsigned int>("window", 'W',
			"initial window size of frequent substring retrieving", false, 3);
	option.add("juman", 'J', "juman-style output");

	option.add<unsigned int>("config", 'C', "config mode", false, 0);

	option.add<unsigned int>("division", 'V', "data/division", false, 1);

	option.add<unsigned int>("N-best", 'N', "N-best output", false, 1);

	option.add<int>("cache", 'H', "cache mode", false, 5);

	option.add<unsigned int>("maxsub", 'U', "utilize maximized substrs", false,
			0);

	option.add<unsigned int>("freeze", 'z', "freeze state trigger", false, 0);
	option.add<int>("decay-gap", 'g', "gap between per decay", false, 100);
	option.add<int>("reflush-period", 'r', "refulsh hash periodically", false, INT_MAX);

	option.add<unsigned int>("threshold", 'o', "freq threshold", false, 1);

	option.add<unsigned int>("bytes", 'Y', "unified byte number", false, 3);
	option.add<unsigned int>("filter", 'T',
			"filtering unreliable boundary estimation", false, 2);

	option.add<int>("printing", 'G', "printing option", false, -1);

	option.add<int>("verbose", 'B', "verbose mode", false, 0); // -1 for silent mode

	option.add<int>("clear", 'Q', "clear structure", false, 0);

//	option.add<std::string>("rdarts", 'E', "reverse darts filename", false,
//			"data/rev.dic.da");
//	option.add<std::string>("rbin", 'X', "reverse dicbin filename", false,
//			"data/rev.dic.bin");
//	option.add<std::string>("rpos", 'Z', "reverse POS list filename", false,
//			"data/rev.dic.pos");

	option.add<unsigned int>("iteration", 'i', "iteration number for training",
			false, 3);
	option.add<unsigned int>("unk_max_length", 'l',
			"maximum length of unknown word detection", false, 3);

	option.add("posfree", 'q', "ignore Part-of-Speech");

	option.add("averaged", 'a', "use averaged perceptron for training");
	option.add("shuffle", 's', "shuffle training data for each iteration");
	option.add("unknown", 'u',
			"apply unknown word detection (obsolete; already default)");
	option.add("debug", '\0', "debug mode");
	option.add("version", 'v', "print version");
	option.add("help", 'h', "print this message");
	option.parse_check(argc, argv);
	if (option.exist("version")) {
		cout << "KKN " << VERSION << endl;
		exit(0);
	}
	if (option.exist("train")) {
		MODE_TRAIN = true;
	}
	if (option.exist("averaged")) {
		WEIGHT_AVERAGED = true;
	}

	if (option.exist("config")) {
		MODE_CONFIG = true;
	}

	if (option.exist("N-best")) {
		MODE_NBEST = true;
	}

	if (option.exist("cache")) {
		MODE_CACHE = true;
	}

	if (option.exist("struct")) {
		MODE_STRUCT = true;
	}

	if (option.exist("filter")) {
		MODE_FILTER = true;
	}

	if (option.exist("maxsub")) {
		MODE_MAXSUB = true;
	}

	if (option.exist("clear")) {
		MODE_CLEAR = true;
	}

	if (option.exist("juman")) {
		MODE_JUMAN = true;
	}

	if (option.exist("posfree")) {
		MODE_POSFREE = true;
	}


}

// write feature weights
bool write_model_file(const std::string &model_filename) {
	std::ofstream model_out(model_filename.c_str(), std::ios::out);
	if (!model_out.is_open()) {
		cerr << ";; cannot open " << model_filename << " for writing" << endl;
		return false;
	}
	for (std::map<std::string, double>::iterator it = feature_weight.begin();
			it != feature_weight.end(); it++) {
		model_out << it->first << " " << it->second << endl;
	}
	model_out.close();
	return true;
}

// read feature weights
bool read_model_file(const std::string &model_filename) {
	std::ifstream model_in(model_filename.c_str(), std::ios::in);
	if (!model_in.is_open()) {
		cerr << ";; cannot open " << model_filename << " for reading" << endl;
		exit(1);
		// return false;
	}
	std::string buffer;
	while (getline(model_in, buffer)) {
		std::vector<std::string> line;
		Morph::split_string(buffer, " ", line);
		feature_weight[line[0]] = atof(
				static_cast<const char *>(line[1].c_str()));
	}
	model_in.close();
	return true;
}

int main(int argc, char** argv) {
	cmdline::parser option;
	option_proc(option, argc, argv);

	Morph::Parameter param(option.get<std::string>("master-path"),
			option.get<std::string>("dics"),
			option.get<std::string>("feature"),
			option.get<std::string>("subfeature"),
			option.get<unsigned int>("iteration"), false,
			option.exist("shuffle"), option.get<unsigned int>("unk_max_length"),
			option.exist("debug"), option.get<unsigned int>("division"),
			option.get<unsigned int>("N-best"),
			option.get<unsigned int>("freeze"),
			option.get<unsigned int>("window"),
			option.get<unsigned int>("bytes"),
			option.get<unsigned int>("config"),
			option.get<unsigned int>("filter"),
			option.get<int>("printing"),
			option.get<unsigned int>("switch"),
			option.get<unsigned int>("threshold"), option.get<int>("verbose"),
			option.get<int>("cache"), option.get<int>("decay-gap"),
			option.get<int>("reflush-period"));

	std::string train_path = option.get<std::string>("train");
	std::string model_path = option.get<std::string>("model");

	std::string gold_standard_path = param.master_path + "/" + option.get<std::string>("gold-standard");
	std::string file_path = param.master_path + "/" + option.get<std::string>("file");
	std::string data_path = param.master_path + "/" + option.get<std::string>("data");
	std::string struct_path = param.master_path + "/" + option.get<std::string>("struct");
	std::string punctuation_path = param.master_path + "/" + option.get<std::string>("punctuation");


	Morph::Tagger tagger(&param);

	tagger.connect_char_nodes();

	if (MODE_TRAIN) {
		if (MODE_MAXSUB) {
			tagger.bytes_unification_checked = tagger.check_bytes_unification(
					file_path,
					punctuation_path);

			tagger.read_gsd_and_load_bound_attrs(
					train_path,
					data_path,
					struct_path,
					punctuation_path);
			tagger.train();
			write_model_file(model_path);
			feature_weight_sum.clear();
		} else {
			tagger.read_gold_data(train_path);
			tagger.train();
			write_model_file(model_path);
			feature_weight_sum.clear();
		}
	} else if (MODE_CONFIG) {
		if (option.get<unsigned int>("config") == 1) {
			tagger.count_freq_segment(file_path,
					data_path,
					struct_path,
					punctuation_path);
		} else if (option.get<unsigned int>("config") == 2) {
			tagger.read_freq_struct_data(data_path,
					punctuation_path);
		} else if (option.get<unsigned int>("config") == 3) {
			tagger.check_bytes_unification(data_path,
					punctuation_path);
		} else if (option.get<unsigned int>("config") == 4) {
			tagger.load_bound_attrs(file_path,
					data_path,
					struct_path,
					punctuation_path);
		} else if (option.get<unsigned int>("config") == 5) {
			int cascading_num = tagger.read_freq_data(
					data_path);
			tagger.cascade_freq_data(data_path,
					cascading_num);
		} else if (option.get<unsigned int>("config") == 6) {
			tagger.read_raw_and_load_bound_attrs(
					file_path,
					data_path,
					struct_path,
					punctuation_path);
		} else if (option.get<unsigned int>("config") == 7) {

			tagger.bytes_unification_checked = tagger.check_bytes_unification(
					file_path,
					punctuation_path);

			tagger.check_feature_in_gsd(
					gold_standard_path,
					data_path,
					struct_path,
					punctuation_path);

		} else if (option.get<unsigned int>("config") == 8) {
			tagger.convert_gsd_to_raw(gold_standard_path);
		} else if (option.get<unsigned int>("config") == 9) {

			tagger.bytes_unification_checked = tagger.check_bytes_unification(
					file_path,
					punctuation_path);

			tagger.clean_struct(
					gold_standard_path,
					data_path,
					struct_path,
					punctuation_path);

		}else if (option.get<unsigned int>("config") == 10) {

			tagger.bytes_unification_checked = tagger.check_bytes_unification(
					file_path,
					punctuation_path);

			tagger.find_upper_bound(
					gold_standard_path,
					file_path,
					data_path,
					struct_path,
					punctuation_path);

			MODE_MAXSUB = true;

			read_model_file(model_path);

			std::ifstream is(argv[1]); // input stream

			// sentence loop
			std::string buffer;
			while (getline(is ? is : cin, buffer)) {

				if (buffer.at(0) == '#') { // empty line or comment line
					cout << buffer << endl;
					continue;
				}

				if (MODE_MAXSUB) {
					tagger.new_sentence_analyze_transparent(buffer,
							num_of_sentences_processed,
							tagger.AryOfBeginBoundAttrsTab,
							tagger.AryOfEndBoundAttrsTab);
				} else {
					tagger.new_sentence_analyze(buffer);
				}

				if (MODE_NBEST) {
					if (MODE_JUMAN)
						tagger.print_Juman_style_path();
					else
						tagger.print_N_best_path();
				} else {
					tagger.print_best_path();
				}
				tagger.sentence_clear();
				++num_of_sentences_processed;
			}

		}else {
			cerr << ";; not implemented" << endl;
		}
	} else {

		if (MODE_CACHE) {
			tagger.count_freq_segment(file_path,
					data_path,
					struct_path,
					punctuation_path);
			boundary_estimation_table = tagger.get_boundary_estimation_table();
		}

		if (MODE_MAXSUB) {
			tagger.bytes_unification_checked = tagger.check_bytes_unification(
					file_path,
					punctuation_path);

			tagger.read_raw_and_load_bound_attrs(
					file_path,
					data_path,
					struct_path,
					punctuation_path);
		}

		cerr << "Reading model: " << model_path << endl;

		read_model_file(model_path);

		std::ifstream is(argv[1]); // input stream

		// sentence loop
		std::string buffer;
		while (getline(is ? is : cin, buffer)) {

			if (buffer.at(0) == '#') { // empty line or comment line
//				cout << buffer << endl;
				continue;
			}

			if (MODE_MAXSUB) {
				tagger.new_sentence_analyze_transparent(buffer,
						num_of_sentences_processed,
						tagger.AryOfBeginBoundAttrsTab,
						tagger.AryOfEndBoundAttrsTab);
			} else {
				tagger.new_sentence_analyze(buffer);
			}

			if (MODE_NBEST) {
				if (MODE_JUMAN)
					tagger.print_Juman_style_path();
				else
					tagger.print_N_best_path();
			} else {
				tagger.print_best_path();
			}
			tagger.sentence_clear();
			++num_of_sentences_processed;

			if (num_of_sentences_processed % 100 == 0)
				cerr << num_of_sentences_processed << endl;

		}
	}

	feature_weight.clear();
	return 0;
}
