#ifndef POS_H
#define POS_H

#include "common.h"

namespace Morph {

class Pos {
    unsigned short count;
    std::map<std::string, unsigned short> dic;
    std::map<unsigned short, std::string> rdic;
  public:
    Pos();

    unsigned short get_id(const std::string &pos_str);
    unsigned short get_id(const char *pos_str);
    std::string *get_pos(unsigned short posid);
    bool write_pos_list(const std::string &pos_filename);
    bool read_pos_list(const std::string &pos_filename);
};

}

#endif
