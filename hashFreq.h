#ifndef HASHFREQ_H_
#define HASHFREQ_H_

#include "common.h"
#include "dic.h"

namespace Morph {

class freqEntity {
	int b_conf; //0: none
	int e_conf; //0: none

public:
	int freq;
	int decayer;
	int strLength; //num of char

	std::deque<std::vector<int> > occurrence; //sentence No. and char pos pairs

	freqEntity() {

	}

	freqEntity(int f, int d, std::deque<std::vector<int> > o, int sL) {
		freq = f;
		decayer = d;
		occurrence = o;
		strLength = sL;
		b_conf = 0;
		e_conf = 0;
	}

	freqEntity(int current_sentence, int current_position, int d, int sL) {
		std::vector<int> tempVec;
		tempVec.push_back(current_sentence);
		tempVec.push_back(current_position);
		occurrence.push_back(tempVec);
		freq = occurrence.size();
		decayer = d;
		strLength = sL;
		b_conf = 0;
		e_conf = 0;
	}

	freqEntity(int d, int sL, int num_of_entries) {

		for (int i = 0; i < num_of_entries; ++i) {
			std::vector<int> tempVec;
			tempVec.push_back(-1);
			tempVec.push_back(-1);
			occurrence.push_back(tempVec);
		}

		freq = occurrence.size();
		decayer = d;
		strLength = sL;
		b_conf = 0;
		e_conf = 0;
	}

	~freqEntity() {

	}

	void update_freq(int num_of_new_entries) {

		for (int i = 0; i < num_of_new_entries; ++i) {
			std::vector<int> tempVec;
			tempVec.push_back(-1);
			tempVec.push_back(-1);
			occurrence.push_back(tempVec);
		}

		freq = occurrence.size();
	}

	void update(int current_sentence, int current_position, int d) {
		std::vector<int> tempVec;
		tempVec.push_back(current_sentence);
		tempVec.push_back(current_position);
		occurrence.push_back(tempVec);
		freq = occurrence.size();
		decayer = d;
	}

	int decay_function(int gap) {
//		if (in_freq < )
		return gap * freq;
	}

	bool decay(int gap) {
		decayer += decay_function(gap);
		occurrence.pop_front();
		freq = occurrence.size();

		if (occurrence.empty()) {
			return true;
		} else {
			return false;
		}
	}

//	void decay(int d) {
//		occurrence.pop_front();
//		freq = occurrence.size();
//		decayer = d;
//	}

	void update_b_conf(int conf) {
		b_conf = conf;
	}

	int get_b_conf() {
		return b_conf;
	}

	void update_e_conf(int conf) {
		e_conf = conf;
	}

	int get_e_conf() {
		return e_conf;
	}

};

class BoundAttrs {

public:
	int begin_pos;
	int end_pos;
	std::string freq_tag;
	std::string max_substr;

	freqEntity* freqEntityRef;
	std::vector<bool> dic_attributes;

	BoundAttrs(freqEntity& in_freqEntity) {
		freqEntityRef = &in_freqEntity;
		for (int i = 0; i < 16; ++i) {
			dic_attributes.push_back(false);
		}
	}

	BoundAttrs(std::string in_maxSubStr, freqEntity& in_freqEntity) {
		max_substr = in_maxSubStr;
		freqEntityRef = &in_freqEntity;
		for (int i = 0; i < 16; ++i) {
			dic_attributes.push_back(false);
		}
	}

	~BoundAttrs() {

	}

};

class indexPrefixTuple {

public:
	std::vector<int>* occurrence_index;
	int prefix_length;
	std::deque<std::vector<int> >::iterator deque_eraser;

	indexPrefixTuple(std::vector<int>* oi, int pl,
			std::deque<std::vector<int> >::iterator de) {
		occurrence_index = oi;
		prefix_length = pl;
		deque_eraser = de;
	}
	;
	~indexPrefixTuple() {

	}
	;

	bool operator<(const indexPrefixTuple &right) const {
		if ((*this).prefix_length < right.prefix_length)
			return true;
		else
			return false;
	}

};

class hashFreq {
	Parameter* param;
	Dic* dic;
	Dic* rev_dic;
	std::map<std::string, int>* punctuation_table;
	std::vector<std::string>* sentences;

	std::map<int, int> freq_counting_list;

public:
	std::map<int, std::string> freq_category_list;

	std::map<std::string, std::map<std::string, Morph::freqEntity> > cacheFreq;

	std::map<std::string,
			std::vector<std::pair<std::vector<int>, std::vector<bool> > > > freqSubAttrs;

	std::map<std::string, std::vector<BoundAttrs> > freqSubAttrsMatrix;

	bool initiated;
	bool closed;
//	int freq;
	int key_length;

	int decay_gap;

	int freq_freeze_state;
	int initial_decay_tolerance;

	hashFreq() {
		closed = false;
		initiated = false;
		initial_decay_tolerance = 200;
	}

	~hashFreq() {

	}

	void init(int kl, std::vector<std::string>& in_sentences,
			std::map<std::string, int>& in_punctuation_table, Dic& in_dic,
			Dic& in_rev_dic, Parameter* in_param, int freeze, int in_decay_gap) {
		key_length = kl;
		freq_freeze_state = freeze;
		sentences = &in_sentences;
		punctuation_table = &in_punctuation_table;
		dic = &in_dic;
		rev_dic = &in_rev_dic;
		param = in_param;
		decay_gap = in_decay_gap;
		initiated = true;
	}

	void init_and_close(int kl,
			std::map<std::string, int>& in_punctuation_table, Dic& in_dic,
			Dic& in_rev_dic, Parameter* in_param, int freeze, int in_decay_gap) {
		key_length = kl;
		freq_freeze_state = freeze;
		punctuation_table = &in_punctuation_table;
		dic = &in_dic;
		rev_dic = &in_rev_dic;
		param = in_param;
		decay_gap = in_decay_gap;
		closed = true;
		initiated = true;
	}

	void open_deck(std::vector<std::string>& in_sentences) {
		sentences = &in_sentences;
	}

//	void fixed_entry(std::string newSeg, int current_sentence,
//			int current_position) {
//		std::map<std::string, Morph::freqEntity>::iterator it = cacheFreq.find(
//				newSeg);
//		std::vector<int> newOccurence;
//		newOccurence.push_back(current_sentence);
//		newOccurence.push_back(current_position);
//
//		if (it != cacheFreq.end()) {
//			it->second.freq++;
//			it->second.occurrence.push_back(newOccurence);
//			it->second.decayer = 0;
//		} else {
//			freqEntity newFreqEntity;
//			newFreqEntity.entity = newSeg;
//			newFreqEntity.freq = 1;
//			newFreqEntity.occurrence.push_back(newOccurence);
//			newFreqEntity.decayer = 0;
//			cacheFreq[newSeg] = newFreqEntity;
//		}
//	}

	int lookup(std::string& newSeg, int current_sentence, int current_position,
			std::map<int, std::map<int, std::vector<int> > >& in_boundary_estimation_table,
			unsigned int in_filter) {

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.find(newSeg);

		if (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator longest_prefix;

			int prefix_length = key_length;

			int reached_length = 0;

			std::string reached_prefix = newSeg;

			bool substring_is_found = false;
			bool freq_candidate_picked_up = false;

			for (std::map<std::string, Morph::freqEntity>::iterator map_iter =
					it->second.begin(); map_iter != it->second.end();
					map_iter++) {

				if (map_iter->first.find(newSeg, 0) == 0) {

					if (map_iter->second.freq > 1) {

						Node* check_beginning = NULL;
						Node* check_ending = NULL;
						Node* check_internal_beginning = NULL;
						Node* check_internal_ending = NULL;

//						std::string ending_char =
//								sentences->at(current_sentence).substr(
//										current_position
//												+ (map_iter->second.strLength
//														- 1)
//														* utf8_bytes(
//																(unsigned char *) sentences->at(
//																		current_sentence).c_str()),
//										utf8_bytes(
//												(unsigned char *) sentences->at(
//														current_sentence).c_str()));
//
//						std::string bi_char_ending_extension =
//								sentences->at(current_sentence).substr(
//										current_position
//												+ (map_iter->second.strLength
//														- 1)
//														* utf8_bytes(
//																(unsigned char *) sentences->at(
//																		current_sentence).c_str()),
//										2
//												* utf8_bytes(
//														(unsigned char *) sentences->at(
//																current_sentence).c_str()));
//
//						std::string tri_char_ending_extension =
//								sentences->at(current_sentence).substr(
//										current_position
//												+ (map_iter->second.strLength
//														- 1)
//														* utf8_bytes(
//																(unsigned char *) sentences->at(
//																		current_sentence).c_str()),
//										3
//												* utf8_bytes(
//														(unsigned char *) sentences->at(
//																current_sentence).c_str()));

						if (current_position
								>= utf8_bytes(
										(unsigned char *) sentences->at(
												current_sentence).c_str())) {
							check_beginning =
									dic->lookup(
											sentences->at(current_sentence).c_str()
													+ current_position
													- utf8_bytes(
															(unsigned char *) sentences->at(
																	current_sentence).c_str()));

							while (check_beginning) {
								if (check_beginning->char_num > 1) {
									if (param->verbose_option == 2)
										cerr << "check beginning hit: "
												<< *(check_beginning->string_for_print)
												<< endl;
									break;
								}
								check_beginning = check_beginning->bnext;
							}

							check_internal_beginning = dic->lookup(
									sentences->at(current_sentence).c_str()
											+ current_position);

							Node* internal_bloop = check_internal_beginning;

							while (internal_bloop) {
								if (internal_bloop->char_num >= in_filter) {

									if (internal_bloop->char_num
											> map_iter->second.strLength)
										cerr
												<< ";;Odd: internal beginning hit exceeds string length: "
												<< *(internal_bloop->string_for_print)
												<< endl;

									if (param->verbose_option == 2)
										cerr << "internal beginning hit: "
												<< *(internal_bloop->string_for_print)
												<< endl;

									break;
								}
								internal_bloop = internal_bloop->bnext;
							}
							if (!internal_bloop) {
								check_internal_beginning = NULL;
							}

						}

						check_ending =
								dic->lookup(
										sentences->at(current_sentence).c_str()
												+ current_position
												+ (map_iter->second.strLength
														- 1)
														* utf8_bytes(
																(unsigned char *) sentences->at(
																		current_sentence).c_str()));

						for (int suffix_length = in_filter;
								suffix_length <= map_iter->second.strLength;
								++suffix_length) {

							check_internal_ending =
									dic->lookup(
											sentences->at(current_sentence).c_str()
													+ current_position
													+ (map_iter->second.strLength
															- suffix_length)
															* utf8_bytes(
																	(unsigned char *) sentences->at(
																			current_sentence).c_str()));

							Node* internal_eloop = check_internal_ending;

							while (internal_eloop) {
								if (internal_eloop->char_num == suffix_length) {
									if (param->verbose_option == 2)
										cerr << "internal ending hit: "
												<< *(internal_eloop->string_for_print)
												<< endl;

									break;
								}
								internal_eloop = internal_eloop->bnext;
							}
							if (!internal_eloop) {
								check_internal_ending = NULL;
							} else {
								break;
							}

						}

						while (check_ending) {
							if (check_ending->char_num > 1) {
								if (param->verbose_option == 2)
									cerr << "check ending hit: "
											<< *(check_ending->string_for_print)
											<< endl;
								break;
							}
							check_ending = check_ending->bnext;
						}

						freq_candidate_picked_up = true;

						int candidate_length = key_length;
						std::string candidate_prefix = newSeg;

						std::string newPrefix =
								sentences->at(current_sentence).substr(
										current_position,
										(map_iter->second.strLength)
												* utf8_bytes(
														(unsigned char *) sentences->at(
																current_sentence).c_str()));

						if (map_iter->first.compare(newPrefix) == 0) {
							candidate_length = map_iter->second.strLength;
							candidate_prefix = newPrefix;
							substring_is_found = true;

							if (in_boundary_estimation_table.find(
									current_sentence)
									!= in_boundary_estimation_table.end()) {

								if (in_boundary_estimation_table[current_sentence].find(
										current_position)
										!= in_boundary_estimation_table[current_sentence].end()) {

									if (in_boundary_estimation_table[current_sentence][current_position][1]
											== 2
											|| in_boundary_estimation_table[current_sentence][current_position][1]
													== -2) {

										in_boundary_estimation_table[current_sentence][current_position][1] =
												0; // ambiguous

									} else if (in_boundary_estimation_table[current_sentence][current_position][1]
											== 0) {
										//I've no idea what to do for now
									} else {

										if (MODE_FILTER && check_beginning) {

											if (check_internal_beginning) {
												in_boundary_estimation_table[current_sentence][current_position][1] =
														1; // unreliable word beginning yet having internal word in dic
											} else {
												in_boundary_estimation_table[current_sentence][current_position][1] =
														-1; // unreliable word beginning
											}

										} else {
											in_boundary_estimation_table[current_sentence][current_position][1] =
													1; // estimation of word beginning
										}

									}

									std::map<int, std::vector<int> >::iterator finder =
											in_boundary_estimation_table[current_sentence].find(
													current_position
															+ (map_iter->second.strLength
																	- 1)
																	* utf8_bytes(
																			(unsigned char *) sentences->at(
																					current_sentence).c_str()));

									if (finder
											!= in_boundary_estimation_table[current_sentence].end()) {
										if (finder->second.at(1) == 2
												|| finder->second.at(1) == -2) {
											if (param->verbose_option == 2)
												cerr << "go on" << endl; //okay;nothing to do for now
										} else {
											cerr
													<< ";;accidentally assigned boundary tag"
													<< endl;
											return -1;
										}
									}

									std::vector<int> newEndAttrPair;
									newEndAttrPair.push_back(
											newPrefix.length());

									if (MODE_FILTER && check_ending) {

										if (check_internal_ending) {
											newEndAttrPair.push_back(2); // unreliable word end yet having internal word in dic
										} else {
											newEndAttrPair.push_back(-2); // unreliable word end
										}

									} else {
										newEndAttrPair.push_back(2); // estimation of word end
									}

									in_boundary_estimation_table[current_sentence][current_position
											+ (map_iter->second.strLength - 1)
													* utf8_bytes(
															(unsigned char *) sentences->at(
																	current_sentence).c_str())] =
											newEndAttrPair;

								} else {

									std::vector<int> newBeginAttrPair;
									std::vector<int> newEndAttrPair;
									newBeginAttrPair.push_back(
											newPrefix.length());
									newEndAttrPair.push_back(
											newPrefix.length());

									if (MODE_FILTER && check_beginning) {

										if (check_internal_beginning) {
											newBeginAttrPair.push_back(1); // unreliable word beginning yet having internal word in dic
										} else {
											newBeginAttrPair.push_back(-1); // unreliable word beginning
										}

									} else {
										newBeginAttrPair.push_back(1); // estimation of word beginning
									}

									in_boundary_estimation_table[current_sentence][current_position] =
											newBeginAttrPair;

									if (MODE_FILTER && check_ending) {

										if (check_internal_ending) {
											newEndAttrPair.push_back(2); // unreliable word end yet having internal word in dic
										} else {
											newEndAttrPair.push_back(-2); // unreliable word end
										}

									} else {
										newEndAttrPair.push_back(2); // estimation of word end
									}

									std::map<int, std::vector<int> >::iterator finder =
											in_boundary_estimation_table[current_sentence].find(
													current_position
															+ (map_iter->second.strLength
																	- 1)
																	* utf8_bytes(
																			(unsigned char *) sentences->at(
																					current_sentence).c_str()));

									if (finder
											!= in_boundary_estimation_table[current_sentence].end()) {
										if (finder->second.at(1) == 2
												|| finder->second.at(1) == -2) {
											if (param->verbose_option == 2)
												cerr << "go on" << endl; //okay;nothing to do for now
										} else {
											cerr
													<< ";;accidentally assigned boundary tag"
													<< endl;
											return -1;
										}
									}

									in_boundary_estimation_table[current_sentence][current_position
											+ (map_iter->second.strLength - 1)
													* utf8_bytes(
															(unsigned char *) sentences->at(
																	current_sentence).c_str())] =
											newEndAttrPair;

								}

							} else {

								std::vector<int> newBeginAttrPair;
								std::vector<int> newEndAttrPair;
								newBeginAttrPair.push_back(newPrefix.length());
								newEndAttrPair.push_back(newPrefix.length());

								if (MODE_FILTER && check_beginning) {

									if (check_internal_beginning) {
										newBeginAttrPair.push_back(1); // unreliable word beginning yet having internal word in dic
									} else {
										newBeginAttrPair.push_back(-1); // unreliable word beginning
									}

								} else {
									newBeginAttrPair.push_back(1); // estimation of word beginning
								}

								if (MODE_FILTER && check_ending) {

									if (check_internal_ending) {
										newEndAttrPair.push_back(2); // unreliable word end yet having internal word in dic
									} else {
										newEndAttrPair.push_back(-2); // unreliable word end
									}

								} else {
									newEndAttrPair.push_back(2); // estimation of word end
								}

								std::map<int, std::vector<int> > newBoundary;

								newBoundary[current_position] =
										newBeginAttrPair;

								newBoundary[current_position
										+ (map_iter->second.strLength - 1)
												* utf8_bytes(
														(unsigned char *) sentences->at(
																current_sentence).c_str())] =
										newEndAttrPair;

								in_boundary_estimation_table[current_sentence] =
										newBoundary;
							}

						}

						//keep this clause for further mod
						if (reached_length < candidate_length) {
							reached_length = candidate_length;
							longest_prefix = map_iter;
							reached_prefix = candidate_prefix;
						}

					} else {
						continue;
					}

				} else {
					cerr << ";;err: prefix not match" << endl;
					return -1;
				}
			}

			if (!substring_is_found) {
				if (param->verbose_option == 2)
					cerr << "all candidates fail to fit current context: "
							<< newSeg << endl;
				return 0;
			} else {
				return 1;
			}

		} else {

			return 0;

		}

	}

	int lookup_and_retrieve(std::string& newSeg, int current_sentence,
			int current_position, unsigned int in_filter) {

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.find(newSeg);

		if (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator longest_prefix;

			int prefix_length = key_length;

			int reached_length = 0;

			std::string reached_prefix = newSeg;

			bool substring_is_found = false;
			bool freq_candidate_picked_up = false;

			for (std::map<std::string, Morph::freqEntity>::iterator map_iter =
					it->second.begin(); map_iter != it->second.end();
					map_iter++) {

				if (map_iter->first.find(newSeg, 0) == 0) {

					std::string newPrefix =
							sentences->at(current_sentence).substr(
									current_position,
									(map_iter->second.strLength)
											* utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str()));

					if (map_iter->first.compare(newPrefix) == 0) {
						if (map_iter->second.freq > 1) {

							cerr << map_iter->first << "\t"
									<< map_iter->first.length() << endl;

							if (map_iter->first.length()
									!= map_iter->second.strLength
											* utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str())) {
								cerr << ";;err: substring length not expected"
										<< endl;
							}

							std::vector<bool> dic_attributes;

							//first entry: meta indicator
							for (int i = 0; i < 16; ++i) {
								dic_attributes.push_back(false);
							}

							freq_candidate_picked_up = collect_bound_attrs(
									dic_attributes, map_iter->first,
									map_iter->second, newSeg, current_sentence,
									current_position, in_filter);

							int candidate_length = key_length;
							std::string candidate_prefix = newSeg;

							candidate_length = map_iter->second.strLength;
							candidate_prefix = newPrefix;
							substring_is_found = true;

							if (MODE_FILTER) {

								if (freqSubAttrs.find(newPrefix)
										== freqSubAttrs.end()) {

									std::vector<int> substr_info;
									substr_info.push_back(
											map_iter->second.freq);
									substr_info.push_back(current_sentence);
									substr_info.push_back(current_position);

									std::vector<
											std::pair<std::vector<int>,
													std::vector<bool> > > substrInfoBox;

									substrInfoBox.push_back(
											std::make_pair(substr_info,
													dic_attributes));

									freqSubAttrs[newPrefix] = substrInfoBox;

								} else {

									std::vector<int> substr_info;
									substr_info.push_back(
											map_iter->second.freq);
									substr_info.push_back(current_sentence);
									substr_info.push_back(current_position);

									freqSubAttrs[newPrefix].push_back(
											std::make_pair(substr_info,
													dic_attributes));
								}

//									if (dic_attributes.at(2)) {
//										if (dic_attributes.at(6) || dic_attributes.at(7) || dic_attributes.at(8))
//											freqSubstrs[newPrefix].at(1) = 10;
//										else
//											freqSubstrs[newPrefix].at(1) = 1;
//									}
//									if (dic_attributes.at(12)) {
//										if (dic_attributes.at(10) || dic_attributes.at(7) || dic_attributes.at(4))
//											freqSubstrs[newPrefix].at(2) = 20;
//										else
//											freqSubstrs[newPrefix].at(2) = 2;
//									}

							}

							//keep this clause for further mod
							if (reached_length < candidate_length) {
								reached_length = candidate_length;
								longest_prefix = map_iter;
								reached_prefix = candidate_prefix;
							}

						}
					}

				} else {
					cerr << ";;err: prefix not match" << endl;
					return -1;
				}
			}

			if (!substring_is_found) {
//				cerr << "all candidates fail to fit current context: " << newSeg
//						<< endl;
				return 0;
			} else {
				return 1;
			}

		} else {

			return 0;

		}

	}

	int entry(std::string& newSeg, int current_sentence, int current_position,
			int round) {

		int step_size = 1;

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.find(newSeg);

		if (it != cacheFreq.end()) {

//			std::set<Morph::freqEntity>::iterator longest_prefix;

//			Morph::freqEntity* longest_prefix;

			std::map<std::string, Morph::freqEntity>::iterator longest_prefix;

			int prefix_length = key_length;

			int reached_length = 0;

			std::string reached_prefix = newSeg;

			bool prefix_is_found = false;

			for (std::map<std::string, Morph::freqEntity>::iterator map_iter =
					it->second.begin(); map_iter != it->second.end();
					map_iter++) {

				if (map_iter->first.find(newSeg, 0) == 0) {

					int candidate_length = key_length;
					std::string candidate_prefix = newSeg;

					std::string newPrefix =
							sentences->at(current_sentence).substr(
									current_position,
									(map_iter->second.strLength)
											* utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str()));

					if (map_iter->first.compare(newPrefix) == 0) {
						candidate_length = map_iter->second.strLength;
						candidate_prefix = newPrefix;
						prefix_is_found = true;
					}

//					while (map_iter->first.find(newPrefix, 0) == 0) {
//
//						candidate_length++;
//						candidate_prefix = newPrefix;
//						newPrefix =
//								sentences->at(current_sentence).substr(
//										current_position,
//										(candidate_length)
//												* utf8_bytes(
//														(unsigned char *) sentences->at(
//																current_sentence).c_str()));
//
//						if (newPrefix.compare(reached_prefix) == 0) {
//							//end of sentence
//							break;
//						}
//
//					}
					if (param->verbose_option == 1)
						cerr << "candidate prefix: " << candidate_prefix
								<< " prefix-in-stock: " << map_iter->first
								<< " " << map_iter->second.occurrence.size()
								<< " in-stock length: "
								<< map_iter->second.strLength << endl;

					if (reached_length < candidate_length) {

						//						longest_prefix = &(map_iter->second);
						reached_length = candidate_length;
						longest_prefix = map_iter;
						reached_prefix = candidate_prefix;
//						reached_prefix =
//								sentences->at(
//										longest_prefix->second.occurrence.front().at(
//												0)).substr(
//										longest_prefix->second.occurrence.front().at(
//												1),
//										reached_length
//												* utf8_bytes(
//														(unsigned char *) sentences->at(
//																current_sentence).c_str()));

					}

				} else {
					cerr << ";;err: prefix not match" << endl;
					return -1;
				}
			}

			if (!prefix_is_found) {
				if (param->verbose_option == 1)
					cerr << "No such prefix... wait." << endl;
			}

			if (param->verbose_option == 1)
				cerr << "longest prefix: " << reached_prefix << "which is "
						<< longest_prefix->first << endl;

			if (reached_length <= key_length) {
				if (param->verbose_option == 1)
					cerr << "now checking... " << reached_prefix << endl;

				std::map<std::string, Morph::freqEntity>::iterator check_exist_iter =
						it->second.find(newSeg);
				if (check_exist_iter == it->second.end()) {
					if (param->verbose_option == 1)
						cerr << "prefix has been erase.  now re-creating."
								<< reached_prefix << endl;

					freqEntity newFreqEntity(current_sentence, current_position,
							round, reached_length);
					std::map<std::string, Morph::freqEntity> tempMap;
					it->second[newSeg] = newFreqEntity;
					return reached_length;
				}
			}

			std::string expand_prefix = reached_prefix;

			int expand_length = reached_length;

			std::deque<std::vector<int> > split_occurrence;
			std::priority_queue<Morph::indexPrefixTuple> heapOfTuples;

			for (std::deque<std::vector<int> >::iterator que_iter =
					longest_prefix->second.occurrence.begin();
					que_iter != longest_prefix->second.occurrence.end();
					que_iter++) {

				std::string newPrefix = reached_prefix;

				int prefix_offset = 0;

				if (sentences->at(que_iter->at(0)).substr(que_iter->at(1),
						(reached_length + prefix_offset)
								* utf8_bytes(
										(unsigned char *) sentences->at(
												current_sentence).c_str())).compare(
						newPrefix) != 0) {

					cerr << ";;err: fail to match expand prefix: " << newPrefix
							<< " vs."
							<< sentences->at(que_iter->at(0)).substr(
									que_iter->at(1),
									(reached_length + prefix_offset)
											* utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str()))
							<< endl;
					return -1;

				} else {

				}

				while (sentences->at(que_iter->at(0)).substr(que_iter->at(1),
						(reached_length + prefix_offset)
								* utf8_bytes(
										(unsigned char *) sentences->at(
												current_sentence).c_str())).compare(
						newPrefix) == 0) {

					expand_prefix = newPrefix;

					if (param->verbose_option == 1)
						cerr << "expand prefix: " << expand_prefix << endl;

					if (punctuation_table->find(
							sentences->at(que_iter->at(0)).substr(
									que_iter->at(1)
											+ (reached_length + prefix_offset
													- 1)
													* utf8_bytes(
															(unsigned char *) sentences->at(
																	current_sentence).c_str()),
									utf8_bytes(
											(unsigned char *) sentences->at(
													current_sentence).c_str())))
							!= punctuation_table->end()) {

						if (param->verbose_option == 1)
							cerr << "ending punc: "
									<< sentences->at(que_iter->at(0)).substr(
											que_iter->at(1)
													+ (reached_length
															+ prefix_offset - 1)
															* utf8_bytes(
																	(unsigned char *) sentences->at(
																			current_sentence).c_str()),
											utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str()))
									<< endl;

						break;

					} else {
						prefix_offset++;

						newPrefix =
								sentences->at(current_sentence).substr(
										current_position,
										(reached_length + prefix_offset)
												* utf8_bytes(
														(unsigned char *) sentences->at(
																current_sentence).c_str()));
					}

					if (newPrefix.compare(expand_prefix) == 0) {
						//end of sentence
						break;
					}

				}

				expand_length = reached_length + prefix_offset - 1;

				Morph::indexPrefixTuple newTuple(&(*que_iter), expand_length,
						que_iter);

				heapOfTuples.push(newTuple);

				if (param->verbose_option == 1)
					cerr << "found longer prefix: " << expand_length << endl;
				//					cerr << "current iter: " <<split_iter->at(1) << endl;
				//					cerr << "heading iter: " <<	longest_prefix->occurrence.begin()->at(1) << endl;

			}

			int max_prefix = heapOfTuples.top().prefix_length;

			//step_size = expand_length;

			if (max_prefix > reached_length) {

				if (param->verbose_option == 1)
					cerr << "entered splitting mode! " << max_prefix << endl;

				while (!heapOfTuples.empty()
						&& heapOfTuples.top().prefix_length >= max_prefix) {
					if (heapOfTuples.top().prefix_length > max_prefix) {
						cerr << ";;err: heap sort error." << endl;
						return -1;
					} else {

						split_occurrence.push_back(
								*(heapOfTuples.top().occurrence_index));

//erase occurrence from original and add to new ms entry

//						cerr << "Before erasing: " << longest_prefix->second.occurrence.size() << endl;

//						longest_prefix->second.occurrence.erase(
//								heapOfTuples.top().deque_eraser);

//						longest_prefix->second.freq =
//								longest_prefix->second.occurrence.size();

//						cerr << "After erasing: " << longest_prefix->second.occurrence.size() << endl;

						heapOfTuples.pop();

						if (longest_prefix->second.occurrence.empty()) {
							cerr
									<< ";;err: longest prefix occurrence list is empty"
									<< endl;
							return -1;
//original code

//							it->second.erase(longest_prefix);
//							break;
						}

					}
				}

//temporary solution
				while (split_occurrence.size() != 1) {
					if (split_occurrence.front().at(0)
							<= split_occurrence.back().at(0))
						split_occurrence.pop_front();
					else
						split_occurrence.pop_back();
				}
//temporary solution end

				if (split_occurrence.size() != 1) {
					cerr
							<< ";;err: singularity violated (extendible not unique) "
							<< endl;
					return -1;

				} else {

					if (freq_freeze_state > 1
							&& round - split_occurrence.back().at(0)
									> decay_function(1)) {
						//no longer splittable; try adding into occrence list in normal way
						bool occurrence_empty = false;

						while ((!occurrence_empty)
								&& (round - longest_prefix->second.decayer
										> longest_prefix->second.decay_function(decay_gap))) {
							occurrence_empty = longest_prefix->second.decay(decay_gap);
						}
						if (!occurrence_empty) {
							longest_prefix->second.update(current_sentence,
									current_position, round);
							step_size = max_prefix;
						} else {
							it->second.erase(longest_prefix);
							if (it->second.empty()) {
								cacheFreq.erase(newSeg);
							}
							//redo to find another match
							step_size = 0;
							return step_size;
						}

					} else {
						std::vector<int> current_coordinate;
						current_coordinate.push_back(current_sentence);
						current_coordinate.push_back(current_position);
						split_occurrence.push_back(current_coordinate);

						Morph::freqEntity split_entity(split_occurrence.size(),
								round, split_occurrence, max_prefix);

						std::string split_prefix =
								sentences->at(current_sentence).substr(
										current_position,
										max_prefix
												* utf8_bytes(
														(unsigned char *) sentences->at(
																current_sentence).c_str()));

						if (param->verbose_option == 1)
							cerr << "split prefix: " << split_prefix << endl;

						std::map<std::string, Morph::freqEntity>::iterator map_insert_iter =
								it->second.find(split_prefix);

						if (map_insert_iter != it->second.end()) {
							cerr << ";;err: prefix already exists." << endl;
							return -1;
						} else {
							it->second[split_prefix] = split_entity;

							if (param->verbose_option == 1)
								cerr << "New prefix has count: "
										<< it->second.find(split_prefix)->second.occurrence.size()
										<< endl;

							step_size = max_prefix;
						}
					}
				}

			} else {

				if (freq_freeze_state > 0
						&& longest_prefix->second.freq < freq_freeze_state) {

					bool occurrence_empty = false;

					if (param->verbose_option == 2)
						cerr << "ready to decay " << longest_prefix->first
								<< " round " << round << " latest decayer "
								<< longest_prefix->second.decayer << " freq "
								<< longest_prefix->second.freq
								<< " decay function"
								<< longest_prefix->second.decay_function(decay_gap)
								<< endl;

					while ((!occurrence_empty)
							&& (round - longest_prefix->second.decayer
									> longest_prefix->second.decay_function(decay_gap))) {

						occurrence_empty = longest_prefix->second.decay(decay_gap);

						if (param->verbose_option == 2)
							cerr << "after a decay " << longest_prefix->first
									<< " round " << round << " latest decayer "
									<< longest_prefix->second.decayer
									<< " freq " << longest_prefix->second.freq
									<< " decay function"
									<< longest_prefix->second.decay_function(decay_gap)
									<< endl;

					}
					if (!occurrence_empty) {
						longest_prefix->second.update(current_sentence,
								current_position, round);
						step_size = max_prefix;
					} else {
						it->second.erase(longest_prefix);
						if (it->second.empty()) {
							cacheFreq.erase(newSeg);
						}
						//redo to find another match
						step_size = 0;
						return step_size;
					}

				} else {
					longest_prefix->second.update(current_sentence,
							current_position, round);
					step_size = max_prefix;
				}

				if (param->verbose_option == 1)
					cerr << "now occurrence is "
							<< longest_prefix->second.occurrence.size() << endl;

				//				std::vector<int> tempVec;
				//				tempVec.push_back(current_sentence);
				//				tempVec.push_back(current_position);
				//				longest_prefix->second.decayer = round;
				//				longest_prefix->second.occurrence.push_back(tempVec);
				//				longest_prefix->second.freq++; //equivalent with expression below
				//				longest_prefix->second.freq =
				//						longest_prefix->second.occurrence.size();

			}

			return step_size;

			//			for (std::deque<std::vector<int> >::iterator it2 =
			//					it->second.occurrence.begin();
			//					it2 != it->second.occurrence.end(); it2++) {
			//				int i = 0;
			//				std::string tempStr = newSeg;
			//
			//				while (sentences->at(it2->at(0)).substr(
			//						it2->at(1),
			//						(it->second.strLength + i)
			//								* utf8_bytes(
			//										(unsigned char *) sentences->at(
			//												it2->at(0)).c_str())).compare(
			//						tempStr) == 0) {
			//					i++;
			//					tempStr = sentences->at(it2->at(0)).substr(
			//							current_position,
			//							(charOffset + i)
			//									* utf8_bytes(
			//											(unsigned char *) sentences->at(
			//													it2->at(0)).c_str()));
			//
			//				}
			//
			//			}

		} else {

			freqEntity newFreqEntity(current_sentence, current_position, round,
					key_length);
			std::map<std::string, Morph::freqEntity> tempMap;
			tempMap[newSeg] = newFreqEntity;
			cacheFreq[newSeg] = tempMap;
		}

		return step_size;
	}

	int check_first_punctuation(std::string& newSeg) {
		int step_size_offset = 0;
		int char_pos = 0;

		for (int i = 0; i < newSeg.length();
				i += utf8_bytes((unsigned char *) (newSeg.c_str() + i))) {

			if (param->verbose_option == 1)
				cerr << "current char: "
						<< newSeg.substr(i,
								utf8_bytes(
										(unsigned char *) (newSeg.c_str() + i)))
						<< endl;

			if (punctuation_table->find(
					newSeg.substr(i,
							utf8_bytes((unsigned char *) (newSeg.c_str() + i))))
					!= punctuation_table->end()) {

				step_size_offset = char_pos + 1;
				break;

			} else {
				char_pos++;
			}

		}

		return step_size_offset;
	}

	void decay_periodical(int round) {

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin();
		while (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin();

			while (map_it != it->second.end()) {

				if (map_it->second.freq < freq_freeze_state) {

					bool clear_and_move_on = false;

					if (param->verbose_option == 2)
						cerr << "ready to decay " << map_it->first << " round "
								<< round << " latest decayer "
								<< map_it->second.decayer << " freq "
								<< map_it->second.freq << " decay function"
								<< map_it->second.decay_function(decay_gap) << endl;

					while ((!clear_and_move_on)
							&& (round - map_it->second.decayer
									> decay_function(map_it->second.freq))) {
						clear_and_move_on = map_it->second.decay(decay_gap);

						if (param->verbose_option == 2)
							cerr << "after a decay " << map_it->first
									<< " round " << round << " latest decayer "
									<< map_it->second.decayer << " freq "
									<< map_it->second.freq << " decay function"
									<< map_it->second.decay_function(decay_gap) << endl;

					}

					if (!clear_and_move_on) {
						map_it++;
					} else {
						it->second.erase(map_it++);
					}

				} else {
					map_it++;
				}

//				if (round - map_it->second.decayer > decay_function(map_it->second.freq)) {
//					if (map_it->second.decay()) {
//						it->second.erase(map_it++);
//					} else {
//						map_it++;
//					}
//				} else {
//					map_it++;
//				}

			}

			if (it->second.empty()) {
				cacheFreq.erase(it++);
			} else {
				it++;
			}

		}

	}

	void decay_instant() {
		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin();
		while (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin();

			while (map_it != it->second.end()) {
				if (map_it->second.decay(decay_gap)) {
					it->second.erase(map_it++);
				} else {
					map_it++;
				}
			}

			if (it->second.empty()) {
				cacheFreq.erase(it++);
			} else {
				it++;
			}

		}

	}

	int decay_function(int in_freq) {
		return decay_gap * in_freq;
	}

	void categorize_freq(int thres) {

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				if (map_it->second.freq > thres) {
					if (freq_counting_list.find(map_it->second.freq)
							!= freq_counting_list.end()) {
						freq_counting_list[map_it->second.freq] += 1;
					} else {
						freq_counting_list[map_it->second.freq] = 1;
					}
				}

				if (map_it->second.freq != map_it->second.occurrence.size()) {
					cerr << ";;err: freq ne size" << endl;
				}

			}

		}

		std::vector<int> freq_temp_container;

		for (std::map<int, int>::iterator count_it = freq_counting_list.begin();
				count_it != freq_counting_list.end(); count_it++) {
			freq_temp_container.push_back(count_it->first);
		}

		for (int i = 0; i < freq_temp_container.size(); ++i) {

			double freq_ratio = double(i + 1)
					/ double(freq_temp_container.size());

			if (freq_ratio < 0.5) {

				cerr << freq_temp_container.at(i) << " LOW" << endl;

				freq_category_list[freq_temp_container.at(i)] = "LOW";

			} else if (freq_ratio < 0.8) {

				cerr << freq_temp_container.at(i) << " MID" << endl;

				freq_category_list[freq_temp_container.at(i)] = "MID";

			} else {

				cerr << freq_temp_container.at(i) << " HIGH" << endl;

				freq_category_list[freq_temp_container.at(i)] = "HIGH";

			}

		}

	}

	bool remove(std::string& prefix, std::string& maxSubStr) {

		if (param->verbose_option == 3)
			cerr << "to remove " << maxSubStr << endl;

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.find(prefix);

		if (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.find(maxSubStr);

			if (map_it != it->second.end()) {

				it->second.erase(map_it);

				if (param->verbose_option == 3)
					cerr << "removed " << map_it->first << endl;

			}

			if (it->second.empty()) {
				cacheFreq.erase(it);
				if (param->verbose_option == 3)
					cerr << "erased " << prefix << endl;

			}

			return true;

		} else {
			return false;
		}

	}

	void print() {
		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				if (map_it->second.freq > 1) {

					for (std::deque<std::vector<int> >::iterator que_iter =
							map_it->second.occurrence.begin();
							que_iter != map_it->second.occurrence.end();
							que_iter++) {

						cout
								<< sentences->at(que_iter->at(0)).substr(
										que_iter->at(1),
										(map_it->second.strLength)
												* utf8_bytes(
														(unsigned char *) sentences->at(
																que_iter->at(0)).c_str()))
								<< "->" << que_iter->at(0) << ","
								<< que_iter->at(1) << "\t";

					}
					cout << endl;
				}

			}

		}
	}

	void print_from_lookup() {

		for (std::map<std::string,
				std::vector<std::pair<std::vector<int>, std::vector<bool> > > >::iterator it =
				freqSubAttrs.begin(); it != freqSubAttrs.end(); ++it) {

			for (std::vector<std::pair<std::vector<int>, std::vector<bool> > >::iterator vec_it =
					it->second.begin(); vec_it != it->second.end(); ++vec_it) {

				cout << it->first << "\t" << vec_it->first.at(0) << ","
						<< vec_it->first.at(1) << "," << vec_it->first.at(2)
						<< "\t";

				for (int i = 0; i < 16; ++i) {
					if (vec_it->second.at(i))
						cout << i << ":1\t";
					else
						cout << i << ":0\t";
				}

				cout << endl;
			}

		}
	}

	void print_freq() {
		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				if (map_it->second.freq > 1) {

					cout << map_it->first << "\t"
							<< map_it->second.occurrence.size() + 1 << endl;

				}

			}

		}
	}

	void print_freq(int thres) {

		int counter = 0;

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				++counter;

				if (map_it->second.freq != map_it->second.occurrence.size()) {
					cerr << ";;err" << endl;
				}

				if (map_it->second.freq > thres) {

					cout << map_it->first << "\t" << map_it->second.strLength
							<< "\t" << map_it->second.occurrence.size() + 1
							<< endl;

				}

			}

		}

		cerr << "counter: " << counter << endl;

	}

	void print_maxsub_alphabet_order(int thres) {

		int counter = 0;

		std::map<std::string, int> ms_alphabet;

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				++counter;

				if (map_it->second.freq != map_it->second.occurrence.size()) {
					cerr << ";;err: freq ne occur_list.size" << endl;
				}

				if (map_it->second.freq > thres) {

					if (ms_alphabet.find(map_it->first) != ms_alphabet.end()) {
						cerr << ";;err: duplicated max-sub in inventory"
								<< endl;
					} else {
						ms_alphabet[map_it->first] = map_it->second.freq;
					}

				}

			}

		}

		cerr << "counter: " << counter << endl;

		for (std::map<std::string, int>::iterator alphabet_it =
				ms_alphabet.begin(); alphabet_it != ms_alphabet.end();
				++alphabet_it) {

//			cout << alphabet_it->first << "\t" << alphabet_it->second << endl;
			cout << alphabet_it->first << endl;

		}

	}

	class freqSubstrObj {

	public:
		std::pair<std::string, Morph::freqEntity> fsObj;

		freqSubstrObj(std::string in_str, Morph::freqEntity in_fe) {
			fsObj = std::make_pair(in_str, in_fe);
		}
		;

		~freqSubstrObj() {

		}
		;

		bool operator<(const freqSubstrObj &right) const {
			return (*this).fsObj.second.freq < right.fsObj.second.freq;
		}

	};

	void print_freq_sorted(int thres) {

		std::priority_queue<freqSubstrObj> myheapbox;

		int counter = 0;

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				++counter;

				if (map_it->second.freq != map_it->second.occurrence.size()) {
					cout << ";;err" << endl;
				}

				if (map_it->second.freq > thres) {

					freqSubstrObj newFsObj(map_it->first, map_it->second);

					myheapbox.push(newFsObj);

				}

			}

		}

		while (!myheapbox.empty()) {

			cout << myheapbox.top().fsObj.first << "\t"
					<< myheapbox.top().fsObj.second.freq << "\t"
					<< myheapbox.top().fsObj.second.strLength << endl;
			myheapbox.pop();

		}

		cerr << "counter: " << counter << endl;

	}

	void print_structure(int thres) {

		int counter = 0;

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			bool foundOne = false;

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				++counter;

				if (map_it->second.freq != map_it->second.occurrence.size()) {
					cerr << ";;err: freq ne size" << endl;
				}

				if (map_it->second.freq > thres) {

					if (!foundOne) {
						cout << it->first << "\t" << key_length << "\t"
								<< it->second.size() << endl;
						foundOne = true;
					}

					if (freq_category_list.find(map_it->second.freq)
							== freq_category_list.end()) {
						cerr << ";;err: freq category not found" << endl;
					}

					cout << "\t" << map_it->first << "\t"
							<< map_it->second.strLength << "\t"
							<< map_it->second.decayer << "\t"
							<< map_it->second.freq << "\t"
							<< freq_category_list[map_it->second.freq] << "\t"
							<< map_it->second.get_b_conf() << "\t"
							<< map_it->second.get_e_conf() << endl;

					for (std::deque<std::vector<int> >::iterator dq_it =
							map_it->second.occurrence.begin();
							dq_it != map_it->second.occurrence.end(); dq_it++) {

						cout << "\t\t" << dq_it->at(0) << "\t" << dq_it->at(1)
								<< endl;

					}

				}

			}

		}

		cout << "#Total number of entries: " << counter << endl;

	}

	void clean_structure(int thres, int in_flag) {

		int counter = 0;

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			bool foundOne = false;

			if (in_flag == 0) {

				for (std::map<std::string, Morph::freqEntity>::iterator map_it =
						it->second.begin(); map_it != it->second.end();
						map_it++) {

					if (map_it->second.get_b_conf() == 1
							|| map_it->second.get_e_conf() == 1) {

						++counter;

						if (map_it->second.freq
								!= map_it->second.occurrence.size()) {
							cerr << ";;err: freq ne size" << endl;
						}

						if (map_it->second.freq > thres) {

							if (!foundOne) {
								cout << it->first << "\t" << key_length << "\t"
										<< it->second.size() << endl;
								foundOne = true;
							}

							if (freq_category_list.find(map_it->second.freq)
									== freq_category_list.end()) {
								cerr << ";;err: freq category not found"
										<< endl;
							}

							cout << "\t" << map_it->first << "\t"
									<< map_it->second.strLength << "\t"
									<< map_it->second.decayer << "\t"
									<< map_it->second.freq << "\t"
									<< freq_category_list[map_it->second.freq]
									<< "\t" << map_it->second.get_b_conf()
									<< "\t" << map_it->second.get_e_conf()
									<< endl;

							for (std::deque<std::vector<int> >::iterator dq_it =
									map_it->second.occurrence.begin();
									dq_it != map_it->second.occurrence.end();
									dq_it++) {

								cout << "\t\t" << dq_it->at(0) << "\t"
										<< dq_it->at(1) << endl;

							}

						}

					}

				}

			} else if (in_flag == 1 || in_flag == 2) {

				for (std::map<std::string, Morph::freqEntity>::iterator map_it =
						it->second.begin(); map_it != it->second.end();
						map_it++) {

					if ( (in_flag == 1 && map_it->second.get_b_conf() == 1)
							|| (in_flag == 2 && map_it->second.get_e_conf() == 1) ) {

						++counter;

						if (map_it->second.freq
								!= map_it->second.occurrence.size()) {
							cerr << ";;err: freq ne size" << endl;
						}

						if (map_it->second.freq > thres) {

							if (!foundOne) {
								cout << it->first << "\t" << key_length << "\t"
										<< it->second.size() << endl;
								foundOne = true;
							}

							if (freq_category_list.find(map_it->second.freq)
									== freq_category_list.end()) {
								cerr << ";;err: freq category not found"
										<< endl;
							}

							cout << "\t" << map_it->first << "\t"
									<< map_it->second.strLength << "\t"
									<< map_it->second.decayer << "\t"
									<< map_it->second.freq << "\t"
									<< freq_category_list[map_it->second.freq]
									<< "\t" << map_it->second.get_b_conf()
									<< "\t" << map_it->second.get_e_conf()
									<< endl;

							for (std::deque<std::vector<int> >::iterator dq_it =
									map_it->second.occurrence.begin();
									dq_it != map_it->second.occurrence.end();
									dq_it++) {

								cout << "\t\t" << dq_it->at(0) << "\t"
										<< dq_it->at(1) << endl;

							}

						}

					}

				}

			} else if (in_flag == 3) {

				for (std::map<std::string, Morph::freqEntity>::iterator map_it =
						it->second.begin(); map_it != it->second.end();
						map_it++) {

					if (map_it->second.get_b_conf() == 1
							&& map_it->second.get_e_conf() == 1) {

						++counter;

						if (map_it->second.freq
								!= map_it->second.occurrence.size()) {
							cerr << ";;err: freq ne size" << endl;
						}

						if (map_it->second.freq > thres) {

							if (!foundOne) {
								cout << it->first << "\t" << key_length << "\t"
										<< it->second.size() << endl;
								foundOne = true;
							}

							if (freq_category_list.find(map_it->second.freq)
									== freq_category_list.end()) {
								cerr << ";;err: freq category not found"
										<< endl;
							}

							cout << "\t" << map_it->first << "\t"
									<< map_it->second.strLength << "\t"
									<< map_it->second.decayer << "\t"
									<< map_it->second.freq << "\t"
									<< freq_category_list[map_it->second.freq]
									<< "\t" << map_it->second.get_b_conf()
									<< "\t" << map_it->second.get_e_conf()
									<< endl;

							for (std::deque<std::vector<int> >::iterator dq_it =
									map_it->second.occurrence.begin();
									dq_it != map_it->second.occurrence.end();
									dq_it++) {

								cout << "\t\t" << dq_it->at(0) << "\t"
										<< dq_it->at(1) << endl;

							}

						}

					}

				}

			}

		}

		cout << "#Total number of entries: " << counter << endl;

	}

	void print_node_stat(Node* in_node) {

		cerr << "Char num: " << in_node->char_num << endl;

		cerr << "length: " << in_node->length << endl;

		cerr << "utf8_bytes: "
				<< utf8_bytes((unsigned char *) in_node->string->c_str())
				<< endl;

	}

	bool collect_bound_attrs(std::vector<bool>& dic_attributes,
			std::string freqSubstr, const freqEntity& upFreqEntity,
			std::string& newSeg, int current_sentence, int current_position,
			unsigned int in_filter) {
		Node* check_beginning = NULL;
		Node* check_ending = NULL;
		Node* check_internal_beginning = NULL;
		Node* check_internal_ending = NULL;
		Node* check_attach_beginning = NULL;
		Node* check_attach_ending = NULL;
		Node* check_overlap_beginning = NULL;
		Node* check_overlap_ending = NULL;

		Node* check_internal_leaveone_beginning = NULL;
		Node* check_internal_leaveone_ending = NULL;
		//						Node* check_internal_ending_rev = NULL;

		//check beginning
		if (current_position == 0) {
			//BOS
			dic_attributes.at(14) = true;
		} else if (current_position
				>= utf8_bytes(
						(unsigned char *) sentences->at(current_sentence).c_str())) {

			check_attach_beginning = rev_dic->bwd_lookup(
					sentences->at(current_sentence).c_str() + current_position,
					current_position);

			while (check_attach_beginning) {
				if (check_attach_beginning->char_num >= in_filter) {

					dic_attributes.at(1) = true;

					if (param->verbose_option == 2)
						cerr << "check attach beginning hit: "
								<< *(check_attach_beginning->string_for_print)
								<< endl;
					break;
				}
				check_attach_beginning = check_attach_beginning->bnext;
			}

			check_beginning = rev_dic->bwd_lookup(
					sentences->at(current_sentence).c_str() + current_position
							+ utf8_bytes(
									(unsigned char *) sentences->at(
											current_sentence).c_str()),
					current_position
							+ utf8_bytes(
									(unsigned char *) sentences->at(
											current_sentence).c_str()));

			while (check_beginning) {
				if (check_beginning->char_num > 1) {

					dic_attributes.at(2) = true;

					if (param->verbose_option == 2)
						cerr << "check beginning hit: "
								<< *(check_beginning->string_for_print) << endl;
					break;
				}
				check_beginning = check_beginning->bnext;
			}

		}

		check_internal_beginning = dic->fwd_lookup(
				sentences->at(current_sentence).c_str() + current_position);

		Node* internal_bloop = check_internal_beginning;

		while (internal_bloop) {
			if (internal_bloop->char_num >= in_filter) {

				if (internal_bloop->char_num < upFreqEntity.strLength) {
					dic_attributes.at(6) = true;
				} else if (internal_bloop->char_num == upFreqEntity.strLength) {
					dic_attributes.at(7) = true;
				} else {
					cerr
							<< ";;Odd: internal beginning hit exceeds string length: "
							<< *(internal_bloop->string_for_print) << endl;
					dic_attributes.at(8) = true;
				}

				if (param->verbose_option == 2)
					cerr << "internal beginning hit: "
							<< *(internal_bloop->string_for_print) << endl;

				//								break;
			}
			internal_bloop = internal_bloop->bnext;
		}
		if (!internal_bloop) {
			check_internal_beginning = NULL;
		}

		for (int prefix_length = 2; prefix_length <= upFreqEntity.strLength - 1;
				++prefix_length) {

			check_overlap_beginning = rev_dic->bwd_lookup(
					sentences->at(current_sentence).c_str() + current_position
							+ prefix_length
									* utf8_bytes(
											(unsigned char *) sentences->at(
													current_sentence).c_str()),
					current_position
							+ prefix_length
									* utf8_bytes(
											(unsigned char *) sentences->at(
													current_sentence).c_str()));

			Node* overlap_bloop = check_overlap_beginning;

			while (overlap_bloop) {
				if (overlap_bloop->char_num >= in_filter) {

					if (overlap_bloop->char_num > prefix_length) {

						dic_attributes.at(3) = true;

					} else if (overlap_bloop->char_num < prefix_length) {

						//obsolete
//						dic_attributes.at(9) = true;

					} else {

						if (!dic_attributes.at(6))
							cerr << ";;err: internal beginning missed " << endl;

					}

					if (param->verbose_option == 2)
						cerr << "overlap beginning hit: "
								<< *(overlap_bloop->string_for_print) << endl;
				}
				overlap_bloop = overlap_bloop->bnext;
			}
			check_overlap_beginning = NULL;
		}

		//check ending
		if (current_position + freqSubstr.length()
				== sentences->at(current_sentence).length()) {
			//EOS
			dic_attributes.at(15) = true;
		} else {

			check_attach_ending = dic->fwd_lookup(
					sentences->at(current_sentence).c_str() + current_position
							+ freqSubstr.length());

			while (check_attach_ending) {
				if (check_attach_ending->char_num >= in_filter) {

					dic_attributes.at(13) = true;

					if (param->verbose_option == 2)
						cerr << "check attach ending hit: "
								<< *(check_attach_ending->string_for_print)
								<< endl;
					break;
				}
				check_attach_ending = check_attach_ending->bnext;
			}

			check_ending = dic->fwd_lookup(
					sentences->at(current_sentence).c_str() + current_position
							+ (upFreqEntity.strLength - 1)
									* utf8_bytes(
											(unsigned char *) sentences->at(
													current_sentence).c_str()));

			while (check_ending) {
				if (check_ending->char_num > 1) {

					dic_attributes.at(12) = true;

					if (param->verbose_option == 2)
						cerr << "check ending hit: "
								<< *(check_ending->string_for_print) << endl;
					break;
				}
				check_ending = check_ending->bnext;
			}

		}

		//

		check_internal_leaveone_ending = rev_dic->bwd_lookup(
				sentences->at(current_sentence).c_str() + current_position
						+ (upFreqEntity.strLength - 1)
								* utf8_bytes(
										(unsigned char *) sentences->at(
												current_sentence).c_str()),
				current_position
						+ (upFreqEntity.strLength - 1)
								* utf8_bytes(
										(unsigned char *) sentences->at(
												current_sentence).c_str()));

		Node* internal_leaveone_eloop = check_internal_leaveone_ending;

		while (internal_leaveone_eloop) {
			if (internal_leaveone_eloop->char_num >= in_filter) {

				if (param->verbose_option == 3)
					cerr << "a9: "
							<< *(internal_leaveone_eloop->string_for_print)
							<< endl;

				dic_attributes.at(9) = true;

				if (internal_leaveone_eloop->char_num
						< upFreqEntity.strLength - 1) {

					//natural

				} else if (internal_leaveone_eloop->char_num
						== upFreqEntity.strLength - 1) {
					//shared with a6 but indicate weak ending estimate
					if (!dic_attributes.at(6)) {
						cerr << ";;err: a6 not detected " << endl;
						print_node_stat(internal_leaveone_eloop);
					}
				} else {
					//shared with a3 but indicate weak ending estimate
					if (!dic_attributes.at(3)) {
						cerr << ";;err: a3 not detected " << endl;
						print_node_stat(internal_leaveone_eloop);
					}
				}

				//								break;
			}
			internal_leaveone_eloop = internal_leaveone_eloop->bnext;
		}
		if (!internal_leaveone_eloop) {
			check_internal_leaveone_ending = NULL;
		}

		//

		check_internal_ending = rev_dic->bwd_lookup(
				sentences->at(current_sentence).c_str() + current_position
						+ upFreqEntity.strLength
								* utf8_bytes(
										(unsigned char *) sentences->at(
												current_sentence).c_str()),
				current_position + freqSubstr.length());

		Node* internal_eloop = check_internal_ending;

		while (internal_eloop) {
			if (internal_eloop->char_num >= in_filter) {

				if (internal_eloop->char_num < upFreqEntity.strLength) {

					dic_attributes.at(10) = true;

				} else if (internal_eloop->char_num == upFreqEntity.strLength) {

					if (!dic_attributes.at(7)) {
						cerr << ";;err: perfect match not detected " << endl;

						print_node_stat(internal_eloop);

					}

				} else {
					dic_attributes.at(4) = true;

					cerr << ";;Odd: internal ending hit exceeds string length: "
							<< *(internal_eloop->string_for_print) << endl;

				}

				if (param->verbose_option == 2)
					cerr << "internal ending hit: "
							<< *(internal_eloop->string_for_print) << endl;

				//								break;
			}
			internal_eloop = internal_eloop->bnext;
		}
		if (!internal_eloop) {
			check_internal_ending = NULL;
		}

		for (int suffix_length = 2; suffix_length <= upFreqEntity.strLength - 1;
				++suffix_length) {

			check_overlap_ending = dic->fwd_lookup(
					sentences->at(current_sentence).c_str() + current_position
							+ (upFreqEntity.strLength - suffix_length)
									* utf8_bytes(
											(unsigned char *) sentences->at(
													current_sentence).c_str()));

			Node* overlap_eloop = check_overlap_ending;

			while (overlap_eloop) {
				if (overlap_eloop->char_num >= in_filter) {

					if (overlap_eloop->char_num > suffix_length) {

						dic_attributes.at(11) = true;

					} else if (overlap_eloop->char_num < suffix_length) {

						//obsolete
//						if (!dic_attributes.at(9))
//							cerr
//									<< ";;err: inside sub missed in forward scanning"
//									<< endl;

					} else {

						if (!dic_attributes.at(10)) {
							cerr << ";;err: internal ending missed " << endl;
							print_node_stat(internal_eloop);
						}

					}

					if (param->verbose_option == 2)
						cerr << "overlap ending hit: "
								<< *(overlap_eloop->string_for_print) << endl;
				}
				overlap_eloop = overlap_eloop->bnext;
			}
			check_overlap_ending = NULL;
		}

		//beginning again

		check_internal_leaveone_beginning = dic->fwd_lookup(
				sentences->at(current_sentence).c_str() + current_position
						+ utf8_bytes(
								(unsigned char *) sentences->at(
										current_sentence).c_str()));

		Node* internal_leaveone_bloop = check_internal_leaveone_beginning;

		while (internal_leaveone_bloop) {
			if (internal_leaveone_bloop->char_num >= in_filter) {

				if (param->verbose_option == 3)
					cerr << "a5: "
							<< *(internal_leaveone_bloop->string_for_print)
							<< endl;

				dic_attributes.at(5) = true;

				if (internal_leaveone_bloop->char_num
						< upFreqEntity.strLength - 1) {

					//natural

				} else if (internal_leaveone_bloop->char_num
						== upFreqEntity.strLength - 1) {
					//shared with a10 but indicate weak beginning estimate
					if (!dic_attributes.at(10)) {
						cerr << ";;err: a10 not detected " << endl;
						print_node_stat(internal_leaveone_bloop);
					}
				} else {
					//shared with a11 but indicate weak beginning estimate
					if (!dic_attributes.at(11)) {
						cerr << ";;err: a11 not detected " << endl;
						print_node_stat(internal_leaveone_bloop);
					}
				}

				//								break;
			}
			internal_leaveone_bloop = internal_leaveone_bloop->bnext;
		}
		if (!internal_leaveone_bloop) {
			check_internal_leaveone_beginning = NULL;
		}

		//						check_internal_ending_rev =
		//								dic->bwd_lookup(
		//										sentences->at(current_sentence).c_str()
		//												+ current_position
		//												+ map_iter->second.strLength
		//														* utf8_bytes(
		//																(unsigned char *) sentences->at(
		//																		current_sentence).c_str()), current_position
		//																		+ map_iter->second.strLength
		//																				* utf8_bytes(
		//																						(unsigned char *) sentences->at(
		//																								current_sentence).c_str()));
		//						int tempCounter = 0;
		//						while (check_internal_ending_rev) {
		//							++tempCounter;
		//							cerr << "hit" << tempCounter << " :"
		//									<< *(check_internal_ending_rev->string_for_print)
		//									<< endl;
		//							check_internal_ending_rev =
		//									check_internal_ending_rev->bnext;
		//						}

		return true;
	}

	bool collect_bound_attrs(std::vector<bool>& dic_attributes,
			std::string freqSubstr, const freqEntity& upFreqEntity,
			std::string& newSeg, std::string& in_sentence, int current_position,
			unsigned int in_filter) {
		Node* check_beginning = NULL;
		Node* check_ending = NULL;
		Node* check_internal_beginning = NULL;
		Node* check_internal_ending = NULL;
		Node* check_attach_beginning = NULL;
		Node* check_attach_ending = NULL;
		Node* check_overlap_beginning = NULL;
		Node* check_overlap_ending = NULL;

		Node* check_internal_leaveone_beginning = NULL;
		Node* check_internal_leaveone_ending = NULL;

		//						Node* check_internal_ending_rev = NULL;

		//check beginning
		if (current_position == 0) {
			//BOS
			if (param->verbose_option == 3)
				cerr << "a14 " << endl;

			dic_attributes.at(14) = true;

		} else if (current_position
				>= utf8_bytes((unsigned char *) in_sentence.c_str())) {

			check_attach_beginning = rev_dic->bwd_lookup(
					in_sentence.c_str() + current_position, current_position);

			while (check_attach_beginning) {
				if (check_attach_beginning->char_num >= in_filter) {

					if (param->verbose_option == 3)
						cerr << "a1: "
								<< *(check_attach_beginning->string_for_print)
								<< endl;

					dic_attributes.at(1) = true;

					if (param->verbose_option == 2)
						cerr << "check attach beginning hit: "
								<< *(check_attach_beginning->string_for_print)
								<< endl;
					break;
				}
				check_attach_beginning = check_attach_beginning->bnext;
			}

			check_beginning = rev_dic->bwd_lookup(
					in_sentence.c_str() + current_position
							+ utf8_bytes((unsigned char *) in_sentence.c_str()),
					current_position
							+ utf8_bytes(
									(unsigned char *) in_sentence.c_str()));

			while (check_beginning) {
				if (check_beginning->char_num > 1) {

					if (param->verbose_option == 3)
						cerr << "a2: " << *(check_beginning->string_for_print)
								<< endl;

					dic_attributes.at(2) = true;

					if (param->verbose_option == 2)
						cerr << "check beginning hit: "
								<< *(check_beginning->string_for_print) << endl;
					break;
				}
				check_beginning = check_beginning->bnext;
			}

		}

		check_internal_beginning = dic->fwd_lookup(
				in_sentence.c_str() + current_position);

		Node* internal_bloop = check_internal_beginning;

		while (internal_bloop) {
			if (internal_bloop->char_num >= in_filter) {

				if (internal_bloop->char_num < upFreqEntity.strLength) {

					if (param->verbose_option == 3)
						cerr << "a6: " << *(internal_bloop->string_for_print)
								<< endl;

					dic_attributes.at(6) = true;

				} else if (internal_bloop->char_num == upFreqEntity.strLength) {

					if (param->verbose_option == 3)
						cerr << "a7: " << *(internal_bloop->string_for_print)
								<< endl;

					dic_attributes.at(7) = true;

				} else {

					if (param->verbose_option == 3)
						cerr << "a8: " << *(internal_bloop->string_for_print)
								<< endl;

					dic_attributes.at(8) = true;

				}

				if (param->verbose_option == 2)
					cerr << "internal beginning hit: "
							<< *(internal_bloop->string_for_print) << endl;

				//								break;
			}
			internal_bloop = internal_bloop->bnext;
		}
		if (!internal_bloop) {
			check_internal_beginning = NULL;
		}

		for (int prefix_length = 2; prefix_length <= upFreqEntity.strLength - 1;
				++prefix_length) {

			check_overlap_beginning =
					rev_dic->bwd_lookup(
							in_sentence.c_str() + current_position
									+ prefix_length
											* utf8_bytes(
													(unsigned char *) in_sentence.c_str()),
							current_position
									+ prefix_length
											* utf8_bytes(
													(unsigned char *) in_sentence.c_str()));

			Node* overlap_bloop = check_overlap_beginning;

			while (overlap_bloop) {
				if (overlap_bloop->char_num >= in_filter) {

					if (overlap_bloop->char_num > prefix_length) {

						if (param->verbose_option == 3)
							cerr << "a3: " << *(overlap_bloop->string_for_print)
									<< endl;

						dic_attributes.at(3) = true;

					} else if (overlap_bloop->char_num < prefix_length) {

						//obsolete
//						if (param->verbose_option == 3)
//							cerr << "a9: " << *(overlap_bloop->string_for_print)
//									<< endl;
//
//						dic_attributes.at(9) = true;

					} else {

						if (!dic_attributes.at(6))
							cerr << ";;err: internal beginning missed " << endl;

					}

					if (param->verbose_option == 2)
						cerr << "overlap beginning hit: "
								<< *(overlap_bloop->string_for_print) << endl;
				}
				overlap_bloop = overlap_bloop->bnext;
			}
			check_overlap_beginning = NULL;
		}

		//check ending
		if (current_position + freqSubstr.length() == in_sentence.length()) {

			if (param->verbose_option == 3)
				cerr << "a15 " << endl;

			//EOS
			dic_attributes.at(15) = true;

		} else {

			check_attach_ending = dic->fwd_lookup(
					in_sentence.c_str() + current_position
							+ freqSubstr.length());

			while (check_attach_ending) {
				if (check_attach_ending->char_num >= in_filter) {

					if (param->verbose_option == 3)
						cerr << "a13: "
								<< *(check_attach_ending->string_for_print)
								<< endl;

					dic_attributes.at(13) = true;

					if (param->verbose_option == 2)
						cerr << "check attach ending hit: "
								<< *(check_attach_ending->string_for_print)
								<< endl;
					break;
				}
				check_attach_ending = check_attach_ending->bnext;
			}

			check_ending =
					dic->fwd_lookup(
							in_sentence.c_str() + current_position
									+ (upFreqEntity.strLength - 1)
											* utf8_bytes(
													(unsigned char *) in_sentence.c_str()));

			while (check_ending) {
				if (check_ending->char_num > 1) {

					if (param->verbose_option == 3)
						cerr << "a12: " << *(check_ending->string_for_print)
								<< endl;

					dic_attributes.at(12) = true;

					if (param->verbose_option == 2)
						cerr << "check ending hit: "
								<< *(check_ending->string_for_print) << endl;
					break;
				}
				check_ending = check_ending->bnext;
			}

		}

		//

		check_internal_leaveone_ending = rev_dic->bwd_lookup(
				in_sentence.c_str() + current_position
						+ (upFreqEntity.strLength - 1)
								* utf8_bytes(
										(unsigned char *) in_sentence.c_str()),
				current_position
						+ (upFreqEntity.strLength - 1)
								* utf8_bytes(
										(unsigned char *) in_sentence.c_str()));

		Node* internal_leaveone_eloop = check_internal_leaveone_ending;

		while (internal_leaveone_eloop) {
			if (internal_leaveone_eloop->char_num >= in_filter) {

				if (param->verbose_option == 3)
					cerr << "a9: "
							<< *(internal_leaveone_eloop->string_for_print)
							<< endl;

				dic_attributes.at(9) = true;

				if (internal_leaveone_eloop->char_num
						< upFreqEntity.strLength - 1) {

					//natural

				} else if (internal_leaveone_eloop->char_num
						== upFreqEntity.strLength - 1) {
					//shared with a6 but indicate weak ending estimate
					if (!dic_attributes.at(6)) {
						cerr << ";;err: a6 not detected " << endl;
						print_node_stat(internal_leaveone_eloop);
					}
				} else {
					//shared with a3 but indicate weak ending estimate
					if (!dic_attributes.at(3)) {
						cerr << ";;err: a3 not detected " << endl;
						print_node_stat(internal_leaveone_eloop);
					}
				}

				//								break;
			}
			internal_leaveone_eloop = internal_leaveone_eloop->bnext;
		}
		if (!internal_leaveone_eloop) {
			check_internal_leaveone_ending = NULL;
		}

		//

		check_internal_ending = rev_dic->bwd_lookup(
				in_sentence.c_str() + current_position
						+ upFreqEntity.strLength
								* utf8_bytes(
										(unsigned char *) in_sentence.c_str()),
				current_position + freqSubstr.length());

		Node* internal_eloop = check_internal_ending;

		while (internal_eloop) {
			if (internal_eloop->char_num >= in_filter) {

				if (internal_eloop->char_num < upFreqEntity.strLength) {

					if (param->verbose_option == 3)
						cerr << "a10: " << *(internal_eloop->string_for_print)
								<< endl;

					dic_attributes.at(10) = true;

				} else if (internal_eloop->char_num == upFreqEntity.strLength) {

					if (!dic_attributes.at(7)) {
						cerr << ";;err: perfect match not detected " << endl;

						print_node_stat(internal_eloop);

					}

				} else {

					if (param->verbose_option == 3)
						cerr << "a4: " << *(internal_eloop->string_for_print)
								<< endl;

					dic_attributes.at(4) = true;

				}

				if (param->verbose_option == 2)
					cerr << "internal ending hit: "
							<< *(internal_eloop->string_for_print) << endl;

				//								break;
			}
			internal_eloop = internal_eloop->bnext;
		}
		if (!internal_eloop) {
			check_internal_ending = NULL;
		}

		for (int suffix_length = 2; suffix_length <= upFreqEntity.strLength - 1;
				++suffix_length) {

			check_overlap_ending =
					dic->fwd_lookup(
							in_sentence.c_str() + current_position
									+ (upFreqEntity.strLength - suffix_length)
											* utf8_bytes(
													(unsigned char *) in_sentence.c_str()));

			Node* overlap_eloop = check_overlap_ending;

			while (overlap_eloop) {
				if (overlap_eloop->char_num >= in_filter) {

					if (overlap_eloop->char_num > suffix_length) {

						if (param->verbose_option == 3)
							cerr << "a11: "
									<< *(overlap_eloop->string_for_print)
									<< endl;

						dic_attributes.at(11) = true;

					} else if (overlap_eloop->char_num < suffix_length) {

						//obsolete
//						if (!dic_attributes.at(9))
//							cerr
//									<< ";;err: inside sub missed in forward scanning"
//									<< endl;

					} else {

						if (!dic_attributes.at(10)) {
							cerr << ";;err: internal ending missed " << endl;
							print_node_stat(internal_eloop);
						}

					}

					if (param->verbose_option == 2)
						cerr << "overlap ending hit: "
								<< *(overlap_eloop->string_for_print) << endl;
				}
				overlap_eloop = overlap_eloop->bnext;
			}
			check_overlap_ending = NULL;
		}

		//beginning again

		check_internal_leaveone_beginning = dic->fwd_lookup(
				in_sentence.c_str() + current_position
						+ utf8_bytes((unsigned char *) in_sentence.c_str()));

		Node* internal_leaveone_bloop = check_internal_leaveone_beginning;

		while (internal_leaveone_bloop) {
			if (internal_leaveone_bloop->char_num >= in_filter) {

				if (param->verbose_option == 3)
					cerr << "a5: "
							<< *(internal_leaveone_bloop->string_for_print)
							<< endl;

				dic_attributes.at(5) = true;

				if (internal_leaveone_bloop->char_num
						< upFreqEntity.strLength - 1) {

					//natural

				} else if (internal_leaveone_bloop->char_num
						== upFreqEntity.strLength - 1) {
					//shared with a10 but indicate weak beginning estimate
					if (!dic_attributes.at(10)) {
						cerr << ";;err: a10 not detected " << endl;
						print_node_stat(internal_leaveone_bloop);
					}
				} else {
					//shared with a11 but indicate weak beginning estimate
					if (!dic_attributes.at(11)) {
						cerr << ";;err: a11 not detected " << endl;
						print_node_stat(internal_leaveone_bloop);
					}
				}

				//								break;
			}
			internal_leaveone_bloop = internal_leaveone_bloop->bnext;
		}
		if (!internal_leaveone_bloop) {
			check_internal_leaveone_beginning = NULL;
		}

		//

		//						check_internal_ending_rev =
		//								dic->bwd_lookup(
		//										sentences->at(current_sentence).c_str()
		//												+ current_position
		//												+ map_iter->second.strLength
		//														* utf8_bytes(
		//																(unsigned char *) sentences->at(
		//																		current_sentence).c_str()), current_position
		//																		+ map_iter->second.strLength
		//																				* utf8_bytes(
		//																						(unsigned char *) sentences->at(
		//																								current_sentence).c_str()));
		//						int tempCounter = 0;
		//						while (check_internal_ending_rev) {
		//							++tempCounter;
		//							cerr << "hit" << tempCounter << " :"
		//									<< *(check_internal_ending_rev->string_for_print)
		//									<< endl;
		//							check_internal_ending_rev =
		//									check_internal_ending_rev->bnext;
		//						}

		return true;
	}

	std::vector<Morph::BoundAttrs*> lookup_and_load_bound_attrs(
			std::string& newSeg, int current_sentence, int current_position,
			unsigned int in_filter) {

		std::vector<Morph::BoundAttrs*> boundAttrsArray;

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.find(newSeg);

		if (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator longest_prefix;

			std::string reached_prefix = newSeg;

			bool freq_candidate_picked_up = false;

			for (std::map<std::string, Morph::freqEntity>::iterator map_iter =
					it->second.begin(); map_iter != it->second.end();
					map_iter++) {

				if (map_iter->first.find(newSeg, 0) == 0) {

					std::string newPrefix =
							sentences->at(current_sentence).substr(
									current_position,
									(map_iter->second.strLength)
											* utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str()));

					if (map_iter->first.compare(newPrefix) == 0) {
						if (map_iter->second.freq > 1) {

							cerr << map_iter->first << "\t"
									<< map_iter->first.length() << "\t"
									<< map_iter->second.freq << endl;

							if (map_iter->first.length()
									!= map_iter->second.strLength
											* utf8_bytes(
													(unsigned char *) sentences->at(
															current_sentence).c_str())) {
								cerr << ";;err: substring length not expected"
										<< endl;
							}

							Morph::BoundAttrs* newBoundAttrs = new BoundAttrs(
									map_iter->first, map_iter->second);

							freq_candidate_picked_up = collect_bound_attrs(
									newBoundAttrs->dic_attributes,
									map_iter->first, map_iter->second, newSeg,
									current_sentence, current_position,
									in_filter);

							if (freq_candidate_picked_up) {

								newBoundAttrs->begin_pos = current_position;
								newBoundAttrs->end_pos = current_position
										+ map_iter->first.length();

								boundAttrsArray.push_back(newBoundAttrs);

							} else {
								cerr
										<< ";;err: unexpected null value returned by collect_bound_attrs"
										<< endl;
							}

						}
					}

				} else {
					cerr << ";;err: prefix not match" << endl;
				}
			}

		}

		return boundAttrsArray;
	}

	std::vector<Morph::BoundAttrs*> lookup_and_load_bound_attrs(
			std::string& newSeg, std::string& in_sentence, int current_position,
			unsigned int in_filter) {

		std::vector<Morph::BoundAttrs*> boundAttrsArray;

		std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.find(newSeg);

		if (it != cacheFreq.end()) {

			std::map<std::string, Morph::freqEntity>::iterator longest_prefix;

			std::string reached_prefix = newSeg;

			bool freq_candidate_picked_up = false;

			for (std::map<std::string, Morph::freqEntity>::iterator map_iter =
					it->second.begin(); map_iter != it->second.end();
					map_iter++) {

				if (map_iter->first.find(newSeg, 0) == 0) {

					std::string newPrefix =
							in_sentence.substr(current_position,
									(map_iter->second.strLength)
											* utf8_bytes(
													(unsigned char *) in_sentence.c_str()));

					if (map_iter->first.compare(newPrefix) == 0) {
						if (map_iter->second.freq > 1) {

							cerr << map_iter->first << "\t"
									<< map_iter->first.length() << "\t"
									<< map_iter->second.freq << endl;

							if (map_iter->first.length()
									!= map_iter->second.strLength
											* utf8_bytes(
													(unsigned char *) in_sentence.c_str())) {
								cerr << ";;err: substring length not expected"
										<< endl;
							}

							Morph::BoundAttrs* newBoundAttrs = new BoundAttrs(
									map_iter->first, map_iter->second);

							freq_candidate_picked_up = collect_bound_attrs(
									newBoundAttrs->dic_attributes,
									map_iter->first, map_iter->second, newSeg,
									in_sentence, current_position, in_filter);

							if (freq_candidate_picked_up) {

								newBoundAttrs->begin_pos = current_position;
								newBoundAttrs->end_pos = current_position
										+ map_iter->first.length();

								boundAttrsArray.push_back(newBoundAttrs);

							} else {
								cerr
										<< ";;err: unexpected null value returned by collect_bound_attrs"
										<< endl;
							}

						}
					}

				} else {
					cerr << ";;err: prefix not match" << endl;
				}
			}

		}

		return boundAttrsArray;
	}

	void load_boudary_estimation_table(
			std::map<int, std::map<int, int> >& in_boundary_estimation_table) {

		for (std::map<std::string, std::map<std::string, Morph::freqEntity> >::iterator it =
				cacheFreq.begin(); it != cacheFreq.end(); it++) {

			for (std::map<std::string, Morph::freqEntity>::iterator map_it =
					it->second.begin(); map_it != it->second.end(); map_it++) {

				if (map_it->second.freq > 1) {

					for (std::deque<std::vector<int> >::iterator dq_it =
							map_it->second.occurrence.begin();
							dq_it != map_it->second.occurrence.end(); dq_it++) {

						std::map<int, std::map<int, int> >::iterator finder;
						if (in_boundary_estimation_table.find(dq_it->at(0))
								!= in_boundary_estimation_table.end()) {

							if (in_boundary_estimation_table[dq_it->at(0)].find(
									dq_it->at(1))
									!= in_boundary_estimation_table[dq_it->at(0)].end()) {

//								cerr
//										<< ";;ambiguous: "
//										<< dq_it->at(0)
//										<< "\t"
//										<< dq_it->at(1)
//										<< "\t"
//										<< in_boundary_estimation_table[dq_it->at(
//												0)][dq_it->at(1)] << endl;

								in_boundary_estimation_table[dq_it->at(0)][dq_it->at(
										1)] = 0; // ambiguous

								in_boundary_estimation_table[dq_it->at(0)][dq_it->at(
										1)
										+ (map_it->second.strLength - 1)
												* utf8_bytes(
														(unsigned char *) sentences->at(
																dq_it->at(0)).c_str())] =
										2; // estimation of word end

							} else {
								in_boundary_estimation_table[dq_it->at(0)][dq_it->at(
										1)] = 1; // estimation of word beginning

								in_boundary_estimation_table[dq_it->at(0)][dq_it->at(
										1)
										+ (map_it->second.strLength - 1)
												* utf8_bytes(
														(unsigned char *) sentences->at(
																dq_it->at(0)).c_str())] =
										2; // estimation of word end
							}

//							cerr
//									<< dq_it->at(0)
//									<< "\t"
//									<< dq_it->at(1)
//									<< "\t"
//									<< sentences->at(dq_it->at(0)).substr(
//											dq_it->at(1),
//											(map_it->second.strLength)
//													* utf8_bytes(
//															(unsigned char *) sentences->at(
//																	dq_it->at(
//																			0)).c_str()))
//									<< endl;

						} else {
							std::map<int, int> newBoundary;
							newBoundary[dq_it->at(1)] = 1; // estimation of word beginning
							newBoundary[dq_it->at(1)
									+ (map_it->second.strLength - 1)
											* utf8_bytes(
													(unsigned char *) sentences->at(
															dq_it->at(0)).c_str())] =
									2; // estimation of word end
							in_boundary_estimation_table[dq_it->at(0)] =
									newBoundary;

//							cerr
//									<< dq_it->at(0)
//									<< "\t"
//									<< dq_it->at(1)
//									<< "\t"
//									<< sentences->at(dq_it->at(0)).substr(
//											dq_it->at(1),
//											(map_it->second.strLength)
//													* utf8_bytes(
//															(unsigned char *) sentences->at(
//																	dq_it->at(
//																			0)).c_str()))
//									<< endl;

						}

					}

//					cout << map_it->first << "\t"
//							<< map_it->second.occurrence.size() + 1 << endl;

				}

			}

		}

	}

};

}

#endif /* HASHFREQ_H_ */
